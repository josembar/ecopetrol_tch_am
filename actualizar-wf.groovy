
//EIM Developer: José Barrantes

log.error("Inicio actualización código FNX WF Documental")

//variable para capturar la carpeta de adjuntos del workflow
def adjuntosFolder

//variable para obtener los atributos del workflow
def atributos

//variable para capturar el estado del workflow
def workflowStatus

//obtener información del workflow
try
{
    
    //obtener el estado del workflow
    //workflowStatus = workflow.getWorkflowStatus(2149182)
    workflowStatus = workflow.getWorkflowStatus(workID)
    
    //obtener los atributos del workflow
    atributos = workflowStatus.getAttributes()
    
    //obtener la carpeta de adjuntos del workflow
    adjuntosFolder = workflowStatus.getAttachmentsFolder()
    
}
catch (e)
{
    log.error("Error consultando informacion del WF Documental: ${}", e)
}

//variable para obtener el dataid del documento que va dentro del workflow
def docID

//variable para obtener el No Expediente
def idCaso

//actualiza el atributo IDCaso del workflow
try
{
    log.error("Actualizando el atributo IDCaso...")
    
    //se guarda el dataid del documento adjunto en el workflow
    docID = docman.getChildrenFast(adjuntosFolder).get(0).ID
    //docID = docman.getChildrenFast(asCSNode(2139472 as long)).get(0).ID
    
    log.error("doc: ${docID}")
    
    //si el adjunto es un documento
    if (asCSNode(docID as long).subtype == 144)
    {
        log.error("El adjunto es un documento...")
        
        idCaso = asCSNode(docID as long)."Solicitud Servicio EC"."No Expediente".toString()
        
    }
    
    //si el adjunto es un shortcut
    if (asCSNode(docID as long).subtype == 1)
    {
        log.error("El adjunto es un shortcut...")
        
        //query para obtener el dataid del documento original
        def queryOrigen = """select origindataid origen
							 from dtree 
							 where dataid = ${docID} 
  							 and deleted = 0
						  """
        //variable para obtener el dataid del documento original
        def docOrigen = sql.runSQLFast(queryOrigen,false,false,0).rows.collect{it.ORIGEN}.get(0)
        
        log.error("docOrigen: ${docOrigen}")
        
        idCaso = asCSNode(docOrigen as long)."Solicitud Servicio EC"."No Expediente".toString()

    }
    
    log.error("idCaso: ${idCaso}")
        
	// se actualiza el atributo IDCaso con la variable idCaso
	atributos.data.IDCaso = idCaso
	workflowStatus.updateData()
    
    log.error("El atributo IDCaso ahora es: ${atributos.data.IDCaso}") 
}

catch (e)
{
    //log.error("Error obteniendo el dataid del binder principal. WF Documental: ${}", e)
    log.error("Error actualizando el atributo IDCaso. WF Documental: ${}", e)
}

//variable para actualizar el título del workflow
def tituloWF

//actualizar título del workflow
try
{
    //se le agrega el número del expediente al título del workflow
    tituloWF = workflowStatus.title.toString()
	tituloWF = "${idCaso} ${tituloWF}"
        
	log.error("tituloWF: ${tituloWF}")
        
	workflow.updateWorkFlowTitle(workID,subWorkID,tituloWF as String)
}

catch (e)
{
    log.error("Error actualizando el título del workflow. WF Documental: ${}", e)
}

log.error("Fin actualización código FNX WF Documental")




