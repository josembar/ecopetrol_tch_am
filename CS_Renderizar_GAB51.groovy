
/*
*
*     \                                   \  |           |       |          
*    _ \   __ \   __|\ \  \   / _ \  __| |\/ |  _ \   _` | |   | |  _ \  __|
*   ___ \  |   |\__ \ \ \  \ /  __/ |    |   | (   | (   | |   | |  __/\__ \
* _/    _\_|  _|____/  \_/\_/ \___|_|   _|  _|\___/ \__,_|\__,_|_|\___|____/
*
*
*/

//EIM Developer: José Barrantes

//variable para capturar la carpeta de adjuntos del workflow
def adjuntosFolder

//variable para obtener los atributos del workflow
//def atributos

//variable para capturar el estado del workflow
def workflowStatus

//variable para capturar el formulario en una tarea del workflow
def formulario

//variable para capturar la tarea de un workflow
def tareaW

//query para obtener título del formulario de la tabla Z_LI_Parametros
def queryTituloDoc = """select DESCRIPCIONLARGA
					 	from Z_LI_Parametros
				   	 	where SIGLA = 'GAB51TIT'
				   	 	and ACTIVO = 1
				  	 """
//el resultado del query queryTituloDoc se le asigna a la variable tituloDoc
def tituloDoc = sql.runSQLFast(queryTituloDoc,false,false,0).rows.collect{it.DESCRIPCIONLARGA}.get(0)

//obtener información del workflow
try
{
    //llamar archivo de constantes
    app = docman.getScriptByNickname("am_logisticainversa_config").getCSVars()?.app
    
    //obtener el estado del workflow
    workflowStatus = workflow.getWorkflowStatus(workID,subWorkID)
    
    //obtener los atributos del workflow
    //atributos = workflowStatus.getAttributes()
    
    //obtener la carpeta de adjuntos del workflow
    adjuntosFolder = workflowStatus.getAttachmentsFolder()
    
    //obtener la tarea del workflow
    tareaWF = workflow.getWorkFlowTask(workID,subWorkID,taskID)
    
    //obtener el formación asociada al paso del workflow
    formulario = forms.getWorkFlowForm(tareaWF,"GAB51")
    //log.error("Formulario: ${formulario}")
}
catch (e)
{
    log.error("GAB51: error consultando informacion del WF: ${workID}", e)
}

//Llenar la vista con información de la solicitud
try
{
    //inserta datos a la vista
    formulario = consultarInformacion(formulario)
    //log.error("formulario: ${formulario}")
}
catch (e)
{
    log.error("GAB51: error adicionando informacion extra en vista de impresion. WF ${workID}", e)
}

//Renderizar el formulario y cargarlo en la carpeta de adjuntos del workflow
try
{

	//variable para capturar el tipo de solicitud
	//def tipoSolicitud = formulario.tipo_Solicitud.value
    
    //variable para capturar la vista correspondiente del formulario
    def vista = docman.getNodeByName(asCSNode(app.constants.templateGAB51 as long),"5_BFGAB51_Print")
    
    //variable para crear objeto renderizable 
    def htmlForm = forms.createRendableForm(formulario,vista)
    
    //variable para renderizar a pdf el objeto creado en la variable anterior
    def pdfDocument = rend.htmlToPdf(htmlForm)
    //log.error("pdfDocument ${pdfDocument}")
    
    //verificar si ya existe un documento renderizado
    def oldDoc = docman.getNodeByName(adjuntosFolder,tituloDoc)
    
    //si existe el documento 
    if (oldDoc)
    {
       //Se añade una nueva versión al documento renderizado
       docman.addVersion(oldDoc,pdfDocument.content)
    }
    
    //si no existe doccumento se crea un documento nuevo
    else
    {
        def newDoc = docman.createDocument(adjuntosFolder,tituloDoc,pdfDocument.content)
    }
    
  	//variable para crear un nuevo documento en la carpeta de adjuntos del workflow
    //def newDoc = docman.createDocument(adjuntosFolder,tituloDoc,pdfDocument.content)
    
    //código para actualizar la fecha de emisión en el registro
    def calendario = Calendar.getInstance()
	def fechaEmision = calendario.getTime().format('dd/MM/yyyy')
    formulario.fechaemision.value = fechaEmision
    forms.updateWorkFlowForm(tareaWF,"GAB51",formulario, false)
}
catch (e)
{
    log.error("GAB51: error renderizando formulario a pdf en el WF: ${workID}", e)
}     

//variable para agregarle información a la vista de impresión
def consultarInformacion (formulario)
{
    //query para obtener título del formulario de la tabla Z_LI_Parametros
	def queryTitulo = """select DESCRIPCIONLARGA
					 	 from Z_LI_Parametros
				   	   	 where SIGLA = 'GAB51TIT'
				   	 	 and ACTIVO = 1
				  	  """
    
    //el resultado del query queryTitulo se le asigna al título del formulario
	formulario.viewParams.title = sql.runSQLFast(queryTitulo,false,false,0).rows.collect{it.DESCRIPCIONLARGA}.get(0)
    
    //variables para obtener el año, mes y dia del sistema
	def calendario = Calendar.getInstance()
	def anio = calendario.get(Calendar.YEAR)
    def mes = calendario.getDisplayName(Calendar.MONTH,Calendar.LONG,new Locale("es"))
    def dia = calendario.get(Calendar.DAY_OF_MONTH)
    // fin de variables para obtener el año, mes y dia del sistema
    
    //variable para obtener el texto equivalente al número de días de la fecha de emisión
    def diaEmisionTexto = obtenerTextoNumero(dia)
    
    //variables para obtener el año, mes y dia de la realización de la subasta
    def fechaSubasta = formulario.fechasubasta.valueAsDate.format("dd/MM/yyyy").toDate()
    def calendarioSubasta = Calendar.getInstance()
    calendarioSubasta.setTime(fechaSubasta)
    def anioSubasta = calendarioSubasta.get(Calendar.YEAR)
    def mesSubasta = calendarioSubasta.getDisplayName(Calendar.MONTH,Calendar.LONG,new Locale("es"))
    def diaSubasta = calendarioSubasta.get(Calendar.DAY_OF_MONTH)
    //fin de variables para obtener el año, mes y dia de la realización de la subasta
    
    //query para extraer los textos para vista de impresión del formulario de la tabla Z_LI_Parametros (Parametrización)
	def queryTextos = """select DESCRIPCIONLARGA
					   	 from Z_LI_Parametros
					   	 where SIGLA = 'GAB51TXT1'
					   	 or SIGLA = 'GAB51TXT2'
					   	 and ACTIVO = 1
					   	 order by SIGLA
					  """
	//obtener el DataID del binder de la solicitud FN (binder papá)
	def binderSolicitudFN = asCSNode(formulario.binderid.value as long).parent.ID
	//log.error("binderSolicitudFN: ${binderSolicitudFN}")

	//obtener el metadato  No Expediente del binder de la solicitud
	def codigoFN = asCSNode(-binderSolicitudFN as long)."Solicitud Servicio LI"."No Expediente".toString()
	//log.error("codigoFN: ${codigoFN}")
    
    //el primer resultado del query queryTextos se le asigna a la variable plantilla1 que equivale a un template
	def plantilla1 = sql.runSQLFast(queryTextos,false,false,0).rows.collect{it.DESCRIPCIONLARGA}.get(0)
    
    //se crea variable contexto1 para llenar las variables ${} que están en el texto plantilla1
    def contexto1 = [
        			 codigoFN: codigoFN,
        			 anioEmision: anio,
        			 mesEmision: mes,
        			 diaEmision: dia,
        			 diaEmisionTexto: diaEmisionTexto,
        			 anioSubasta: anioSubasta,
        			 mesSubasta: mesSubasta,
        			 diaSubasta: diaSubasta,
        			 subasta: formulario.subasta.value,
        			 oferente: formulario.oferente.value,
                     oferenteID: formulario.oferenteid.value
                    ]
    
    //se crea variable texto1 que contiene el texto de plantilla1 con las variables ${} llenas
    def texto1 = template.evaluateTemplate(plantilla1,contexto1)
    
    //Se iguala el custom HTML Texto1 del formulario con la variable texto1
	formulario.viewParams.Texto1 = texto1
    
	//variable para obtener el texto equivalente al número de días de plazo de pago
    def plazoPagoTexto = obtenerTextoNumero(formulario.plazopago.value as int)
    //log.error("plazoPagoTexto: ${plazoPagoTexto}")
    
    //variables para obtener el número de días hábiles entre el plazo inicial (plazo desde) y el plazo final (plazo hasta)
    //def plazoDesde = formulario.plazodesde.value.toDate().format("dd/MM/yyyy")
    def plazoDesde = formulario.plazodesde.valueAsDate.format("dd/MM/yyyy")
    //log.error("plazodesde f: ${formulario.plazodesde.value}")
    //log.error("plazoDesde: ${plazoDesde}")
    def plazoHasta = formulario.plazohasta.valueAsDate.format("dd/MM/yyyy")
    //log.error("plazohasta f: ${formulario.plazohasta.value}")
    //log.error("plazoHasta: ${plazoHasta}")
    def plazoRetiro = intervaloDias(plazoDesde,plazoHasta)
    //log.error("plazoRetiro: ${plazoRetiro}")
    //fin de variables para obtener el número de días hábiles entre el plazo inicial y el plazo final
    
    //variable para obtener el texto equivalente al número de días de plazo de retiro
    def plazoRetiroTexto = obtenerTextoNumero(plazoRetiro as int)
    //log.error("plazoRetiroTexto: ${plazoRetiroTexto}")
    
    //variable para obtener la cifra formateada del valor totalMasIVA
    totalMasIVATexto = formatoCifra (new BigDecimal(formulario.totalmasiva.value))
    //log.error("totalMasIVAFormato: ${totalMasIVAFormato}")
    
    //obtener el metadato  Lugar recoleccion del binder de la solicitud
	def lugarRetiro = asCSNode(-binderSolicitudFN as long)."Solicitud Servicio LI"."Lugar recoleccion".toString()
	log.error("lugarRetiro: ${lugarRetiro}")

    //el segundo resultado del query queryTitulo se le asigna a la variable plantilla2 que equivale a un template
	def plantilla2 = sql.runSQLFast(queryTextos,false,false,0).rows.collect{it.DESCRIPCIONLARGA}.get(1)
    
    //se crea variable contexto2 para llenar las variables ${} que están en el texto plantilla2
    def contexto2 = [
        			 //lote: formulario.detalle.lote.value,
        			 //subasta: formulario.subasta.value,
        			 //cantAprox: formulario.detalle.cantaprox.value,
        			 //unidad: formulario.detalle.unidad.value,
        			 //descripcion: formulario.detalle.descripcion.value,
        			 //vupma: formulario.detalle.vupma.value,
        			 //totalAntesIVA: formulario.totalantesiva.value,
        			 //totalMasIVA: formulario.totalmasiva.value,
        			 totalMasIVATexto: totalMasIVATexto,
        			 lugarRetiro: lugarRetiro,
        			 oferente: formulario.oferente.value,
                     oferenteID: formulario.oferenteid.value,
        			 cuentaAhorro: formulario.cuentaahorro.value,
        			 cuentaBanco: formulario.cuentabanco.value,
        			 titularCuenta: formulario.titularcuenta.value,
        			 titularID: formulario.titularid.value,
        			 plazoPagoTexto: plazoPagoTexto,
        			 plazoPago: formulario.plazopago.value,
        			 plazoRetiroTexto: plazoRetiroTexto,
        			 plazoRetiro: plazoRetiro
                    ]
    
    //se crea variable texto2 que contiene el texto de plantilla2 con las variables ${} llenas
    def texto2 = template.evaluateTemplate(plantilla2,contexto2)
    
    //Se iguala el custom HTML Texto2 del formulario con el segundo resultado de queryTextos
    formulario.viewParams.Texto2 = texto2
    
    //se retorna formulario
    return formulario
}

//código para obtener el texto equivalente al número de días de la fecha de emisión
def obtenerTextoNumero (int dias)
{
    //variable para almacenar el texto equivalente al número
    String nTexto
    //arreglo con los texto que son únicos para los números de 1 a 15, además del 20
    String[] unicos = [
        				"","un","dos","tres","cuatro","cinco","seis","siete","ocho",
        				"nueve","diez","once","doce","trece","catorce","quince","",
        				"","","","veinte"
    				  ]
    //areglo con los texto para los número cuya división por 10 no genera residuo
    String[] decenas = [
        				"","dieci","veinti","treinta","cuarenta","cincuenta",
        				"sesenta","setenta","ochenta","noventa"
    				   ]
    
    //obtener texto números de 1 a 15, además del 20
    if (dias <= 15 || dias == 20)
    {
        nTexto = unicos[dias]
    }
    
    //obtener texto números de 16 a 29, menos el 20
    else if (15 < dias && dias <= 29 && dias != 20)
    {
        nTexto = decenas[dias/10] + unicos[dias % 10]
    }
    
    //obtener texto número 30, 40, 50, 60, 70, 80, 90
    else if (dias >= 30 && dias % 10 == 0 && dias < 100)
    {
        nTexto = decenas[dias/10]
    }
    
    //obtener números: 31 a 39, 41 a 49, 51 a 59, 61 a 69, 71 a 79, 81 a 89, 91 a 99
    else if (dias >= 30 && dias % 10 > 0 && dias < 100)
    {
        nTexto = decenas[dias/10] + " y " + unicos[dias % 10]
    }
    
    //mayor a 99
    else
    {
        nTexto = "No se pudo obtener el texto equivalente"
    }
    
    //se retorna el texto obtenido
    return nTexto
}

//codigo para obtener el número de días hábiles entre el plazo inicial (plazo desde) y el plazo final (plazo hasta)
def intervaloDias (String inicio,String fin)
{
    //llamar archivo de constantes
    app = docman.getScriptByNickname("am_logisticainversa_config").getCSVars()?.app
    
    //se ejecuta LR que ejecuta una función que calcula los días hábiles
    def diasHabiles = docman.runReport(app.constants.diasHabilesLR as long,inicio,fin)
    				  .rows.collect{it.DIAS_HABILES}.get(0)
    
    //También se puede ejecutar la función como sentencia SQL
    //def queryDiasHabiles = """select fn_ecp_dias_habiles('${inicio}','${fin}') as dias_habiles from dual"""
    //def diasHabiles = sql.runSQLFast(queryDiasHabiles,false,false,0).rows.collect{it.DIAS_HABILES}.get(0)
    
    //se retorna el resultado obtenido
    return diasHabiles
}

//función para dar formato de dinero a valores numéricos
def formatoCifra (BigDecimal numero)
{
    def cifraFormato = "\$${String.format('%,.2f',numero)}".replace(',',"'")
    //.toString()
    return cifraFormato
}





