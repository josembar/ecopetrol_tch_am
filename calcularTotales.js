/*
EIM Developer: José Barrantes
Función para calcular los valores totales (valor total cantidad aproximada * valor unidad)
(total con IVA)
*/
//VUPMA = VALOR UNITARIO POSTURA MÁS ALTA
//VTPLPMA = VALOR TOTAL POR LOTE POSTURA MÁS ALTA
function calcularTotales ()
{
    //variable para almacenar el porcentaje del IVA que está en tabla de parametrización (ver onload de la vista del formulario)
    //var IVA = Number($("#IVA").val());
    //console.log(`IVA: ${IVA}`)
    
    //variable para calcular total sin IVA
    var totalAntesIVA = 0;
    
    //variable para calcular el monto IVA
    var montoIVA = 0;
    
    //variable para calcular total más IVA
    var totalMasIVA = 0;
    
    //variables para extraer los atributos del campo VTPLPMA del template
    //variable para extraer el prefijo que va a llevar el valor, ejemplos: $, €
    //var prefijo = $(".detallerequisicionVTPLPMA input[name^='amcurrency_']").attr("data-prefix");
    var prefijo = $(".detallerequisicionVTPLPMA p").attr("data-prefix");
    //console.log(`Prefijo: ${prefijo}`)
    
    //variable para extraer el separador de centécimas (decimales) data-cents-separator
    //var separadorCent = $(".detallerequisicionVTPLPMA input[name^='amcurrency_']").attr("data-cents-separator");
    var separadorCent = $(".detallerequisicionVTPLPMA p").attr("data-cents-separator");
    //console.log(`Separador de centécimas: ${separadorCent}`)
    
    //variable para extraer el separador de miles, ej: 1000 sería 1'000.00 data-thousands-separator
    //var separadorMil = $(".detallerequisicionVTPLPMA input[name^='amcurrency_']").attr("data-thousands-separator");
    var separadorMil = $(".detallerequisicionVTPLPMA p").attr("data-thousands-separator");
    //console.log(`Separador de miles: ${separadorMil}`)
    
    //variable para extraer el máximo de centécimas data-cents-limit
    //var maximoCent = Number($(".detallerequisicionVTPLPMA input[name^='amcurrency_']").attr("data-cents-limit"));
    var maximoCent = Number($(".detallerequisicionVTPLPMA p").attr("data-cents-limit"));
    //console.log(maximoCent)
    
    //variable para extraer clearOnEmpty data-clear-on-empty
    //var clearonempty = $(".detallerequisicionVTPLPMA input[name^='amcurrency_']").attr("data-clear-on-empty");
    var clearonempty = $(".detallerequisicionVTPLPMA p").attr("data-clear-on-empty").toLowerCase() == 'true' ? true : false;
    //console.log(clearonempty)
    
    //fin de variables para extraer los atributos del campo VTPLPMA del template

    //calcular totalVTPLPMA
    for (var i = 0; i < $(".detallerequisicionCantAprox input[name^='amcurrency_']").size(); i++) 
    {   
        //variable para almacenar la cantidad aproximada
        var cantidadAproximada = 0;
		cantidadAproximada = Number($(".detallerequisicionCantAprox input[name^='amcurrency_']").eq(i).val().replace(/[^0-9\.-]+/g,""));
        cantidadAproximada = isNaN(cantidadAproximada)?0:cantidadAproximada;
        //console.log(`Cantidad aproximada: ${cantidadAproximada}`)
        
        //variable para almacenar el equivalente en texto de la variable cantidadAproximada
        var cantidadAproximadaTxt = cantidadAproximada.toLocaleString('en-US',{style:'currency',currency:'USD'});
        cantidadAproximadaTxt = cantidadAproximadaTxt.replace(/,/g,"'").replace('$','');
        //console.log(`cantidadAproximadaTxt: ${cantidadAproximadaTxt}`)
        
        //se le asigna el valor de cantidadAproximadaTxt al campo oculto del Custom HTML del formulario
        $(".am-form-custom-html > .cantaproxtxt").eq(i).val(cantidadAproximadaTxt);

        //variable para almacenar el valor unitario VUPMA
        var valorUnitario = 0;
		valorUnitario = Number($(".detallerequisicionVUPMA input[name^='amcurrency_']").eq(i).val().replace(/[^0-9\.-]+/g,""));
        valorUnitario = isNaN(valorUnitario)?0:valorUnitario;
        //console.log(`Valor unitario: ${valorUnitario}`)
        
        //variable para almacenar el equivalente en texto de la variable valorUnitario
        var valorUnitarioTxt = valorUnitario.toLocaleString('en-US',{style:'currency',currency:'USD'});
        valorUnitarioTxt = valorUnitarioTxt.replace(/,/g,"'");
        //console.log(`valorUnitarioTxt: ${valorUnitarioTxt}`)
        
        //se le asigna el valor de valorUnitarioTxt al campo oculto del Custom HTML del formulario
        $(".am-form-custom-html > .vupmatxt").eq(i).val(valorUnitarioTxt);

        //variable para calcular el total VTPLPMA (VUPMA * Cantidad aproximada)
        var totalPosturaMasAlta = 0;
		totalPosturaMasAlta = Math.round(cantidadAproximada * valorUnitario);
        totalPosturaMasAlta = isNaN(totalPosturaMasAlta)?0:totalPosturaMasAlta;
        //console.log(`Total postura más alta: ${totalPosturaMasAlta}`)
        
        //variable para almacenar el equivalente en texto de la variable totalPosturaMasAlta
        var totalPosturaMasAltaTxt = totalPosturaMasAlta.toLocaleString('en-US',{style:'currency',currency:'USD'});
        totalPosturaMasAltaTxt = totalPosturaMasAltaTxt.replace(/,/g,"'");
        //console.log(`totalPosturaMasAltaTxt: ${totalPosturaMasAltaTxt}`)
        
        //se le asigna el valor de valorUnitarioTxt al campo oculto del Custom HTML del formulario
        $(".am-form-custom-html > .vtplpmatxt").eq(i).val(totalPosturaMasAltaTxt);

        //se van sumando los totales de cada row en la variable totalAntesIVA
        totalAntesIVA += Number(totalPosturaMasAlta);
        //console.log(`totalAntesIVA: ${totalAntesIVA}`)
        // variable para obtener el equivalente en formato divisa de la variable totalAntesIVA
        var totalAntesIVATxt = totalAntesIVA.toLocaleString('en-US',{style:'currency',currency:'USD'});
        totalAntesIVATxt = totalAntesIVATxt.replace(/,/g,"'");
        //console.log(`totalAntesIVATxt: ${totalAntesIVATxt}`)

        //se le asigna el valor de totalPosturaMasAlta al campo oculto del formulario
        $(".detallerequisicionVTPLPMA input[type='hidden']").eq(i).val(totalPosturaMasAlta.toFixed(maximoCent));
        
        //se le asigna el valor de totalPosturaMasAlta al campo visible del formulario
        //$(".detallerequisicionVTPLPMA input[name^='amcurrency_']").eq(i).val(totalPosturaMasAlta.toFixed(maximoCent));
        $(".detallerequisicionVTPLPMA p").eq(i).text(totalPosturaMasAlta.toFixed(maximoCent));
        
        //se le asigna el valor de totalPosturaMasAltaTxt al campo oculto del formulario
        //$("#$form.detalle.vtplpmatxt.id").val(totalPosturaMasAltaTxt)
        
        //al cambiar el valor del campo cantidad aproximada se estaba perdiendo el prefijo del campo VTPLPMA 
        //$(".detallerequisicionVTPLPMA input[name^='amcurrency_']").eq(i).priceFormat({
        $(".detallerequisicionVTPLPMA p").eq(i).priceFormat({
        //el prefijo se iguala al del campo VTPLPMA
    	prefix: prefijo,
        //el separador de centécimas se iguala al atributo del campo VTPLPMA
    	centsSeparator: separadorCent,
        //el separador de miles se iguala al atributo del campo VTPLPMA
    	thousandsSeparator: separadorMil,
        //el máximo de centécimas se iguala al atributo del campo VTPLPMA
    	centsLimit: maximoCent,
        //el clearonempty se iguala al atributo del campo VTPLPMA
        clearOnEmpty: clearonempty
		});	

        //se le asigna el valor de totalAntesIVA al campo oculto del formulario
        $(".totalAntesIVA input[type='hidden']").val(totalAntesIVA.toFixed(maximoCent));
        
        //se le asigna el valor de totalAntesIVA al campo visible del formulario
        $(".totalAntesIVA p").text(totalAntesIVA.toFixed(maximoCent));
        //console.log(`Total antes IVA: ${totalAntesIVA}`)
        
        //se le asigna el valor de totalAntesIVATxt al campo oculto del formulario
        $("#$form.totalantesivatxt.id").val(totalAntesIVATxt)
        
        //el campo total antes IVA estaba perdiendo el prefijo $, este código arregla ese problema
        $(".totalAntesIVA p").priceFormat({
        //el prefijo se iguala al del campo VTPLPMA
    	prefix: prefijo,
        //el separador de centécimas se iguala al atributo del campo VTPLPMA
    	centsSeparator: separadorCent,
        //el separador de miles se iguala al atributo del campo VTPLPMA
    	thousandsSeparator: separadorMil,
        //el máximo de centécimas se iguala al atributo del campo VTPLPMA
    	centsLimit: maximoCent,
        //el clearonempty se iguala al atributo del campo VTPLPMA
        clearOnEmpty: clearonempty
		});
        
		//cálculo del monto IVA
        //montoIVA = totalAntesIVA * IVA
        montoIVA = Number($(".montoIVA input[type='hidden']").val().replace(/[^0-9\.-]+/g,""));
        //console.log(`montoIVA: ${montoIVA}`)
        // variable para obtener el equivalente en formato divisa de la variable montoIVA 
        var montoIVATxt = montoIVA.toLocaleString('en-US',{style:'currency',currency:'USD'});
        montoIVATxt = montoIVATxt.replace(/,/g,"'");
        //console.log(`montoIVATxt: ${montoIVATxt}`)
                    
        //cálculo del total más IVA
        //totalMasIVA = totalAntesIVA + (totalAntesIVA * IVA)
        totalMasIVA = totalAntesIVA + montoIVA
        //console.log(`totalMasIVA: ${totalMasIVA}`)
        // variable para obtener el equivalente en formato divisa de la variable totalMasIVA 
        var totalMasIVATxt = totalMasIVA.toLocaleString('en-US',{style:'currency',currency:'USD'});
        totalMasIVATxt = totalMasIVATxt.replace(/,/g,"'");
        //console.log(`totalMasIVATxt: ${totalMasIVATxt}`)
        
        //se le asigna el valor de montoIVA al campo oculto del formulario
        //$(".montoIVA input[type='hidden']").val(montoIVA.toFixed(maximoCent));
        
        //se le asigna el valor de montoIVA al campo visible del formulario
        //$(".montoIVA p").text(montoIVA.toFixed(maximoCent));
        //$(".montoIVA input[name^='amcurrency_']").text(montoIVA.toFixed(maximoCent));
        //console.log(`MontoIVA: ${montoIVA}`)
        
        //se le asigna el valor de montoIVATxt al campo oculto del formulario
        $("#$form.montoivatxt.id").val(montoIVATxt)
        
        //el campo total más IVA estaba perdiendo el prefijo $, este código arregla ese problema
        //$(".montoIVA p").priceFormat({
        $(".montoIVA input[name^='amcurrency_']").priceFormat({
        //el prefijo se iguala al del campo VTPLPMA
    	prefix: prefijo,
        //el separador de centécimas se iguala al atributo del campo VTPLPMA
    	centsSeparator: separadorCent,
        //el separador de miles se iguala al atributo del campo VTPLPMA
    	thousandsSeparator: separadorMil,
        //el máximo de centécimas se iguala al atributo del campo VTPLPMA
    	centsLimit: maximoCent,
        //el clearonempty se iguala al atributo del campo VTPLPMA
        clearOnEmpty: clearonempty
		});
        
        //se le asigna el valor de totalMasIVA al campo oculto del formulario
        $(".totalMasIVA input[type='hidden']").val(totalMasIVA.toFixed(maximoCent));
        
        //se le asigna el valor de totalMasIVA al campo visible del formulario
        $(".totalMasIVA p").text(totalMasIVA.toFixed(maximoCent));
        //console.log(`Total más IVA: ${totalMasIVA}`)
        
        //se le asigna el valor de totalMasIVATxt al campo oculto del formulario
        $("#$form.totalmasivatxt.id").val(totalMasIVATxt)
        
        //el campo total más IVA estaba perdiendo el prefijo $, este código arregla ese problema
        $(".totalMasIVA p").priceFormat({
        //el prefijo se iguala al del campo VTPLPMA
    	prefix: prefijo,
        //el separador de centécimas se iguala al atributo del campo VTPLPMA
    	centsSeparator: separadorCent,
        //el separador de miles se iguala al atributo del campo VTPLPMA
    	thousandsSeparator: separadorMil,
        //el máximo de centécimas se iguala al atributo del campo VTPLPMA
    	centsLimit: maximoCent,
        //el clearonempty se iguala al atributo del campo VTPLPMA
        clearOnEmpty: clearonempty
		});
    }
}

$(function(){
    //Cuando cargue la pagina calcula los totales
    calcularTotales();
});