/*
████████╗███████╗ ██████╗██╗  ██╗███████╗██████╗  ██████╗ ███████╗    ████████╗███████╗ █████╗ ███╗   ███╗
╚══██╔══╝██╔════╝██╔════╝██║  ██║██╔════╝██╔══██╗██╔════╝ ██╔════╝    ╚══██╔══╝██╔════╝██╔══██╗████╗ ████║
   ██║   █████╗  ██║     ███████║█████╗  ██║  ██║██║  ███╗█████╗         ██║   █████╗  ███████║██╔████╔██║
   ██║   ██╔══╝  ██║     ██╔══██║██╔══╝  ██║  ██║██║   ██║██╔══╝         ██║   ██╔══╝  ██╔══██║██║╚██╔╝██║
   ██║   ███████╗╚██████╗██║  ██║███████╗██████╔╝╚██████╔╝███████╗       ██║   ███████╗██║  ██║██║ ╚═╝ ██║
   ╚═╝   ╚══════╝ ╚═════╝╚═╝  ╚═╝╚══════╝╚═════╝  ╚═════╝ ╚══════╝       ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝     ╚═╝
                                            ╦╔╗╔╔═╗╔═╗╦╦═╗╦╔╗╔╔═╗  ╔╦╗╦═╗╦ ╦╔═╗╔╦╗  ╔═╗╦  ╔═╗╔╗ ╔═╗╦  ╦ ╦ ╦
                                            ║║║║╚═╗╠═╝║╠╦╝║║║║║ ╦   ║ ╠╦╝║ ║╚═╗ ║   ║ ╦║  ║ ║╠╩╗╠═╣║  ║ ╚╦╝
                                            ╩╝╚╝╚═╝╩  ╩╩╚═╩╝╚╝╚═╝   ╩ ╩╚═╚═╝╚═╝ ╩   ╚═╝╩═╝╚═╝╚═╝╩ ╩╩═╝╩═╝╩ 
*/
/***** Reporte Excel GAB248- José Barrantes 27/03/2019 BEGIN *****/


import java.text.SimpleDateFormat
import java.util.Date

//Nombre pestañas
VALORES = "VALORES_UNICOS"
REVISOR = "REVISOR"
APROBADOR = "APROBADOR"
SECCIONB = "CONCEPTO ESPECIALISTAS"
SECCIONC = "RESULTADOS DEL ANÁLISIS"
SECCIONCGASTOS = "GASTOS"
SECCIONCPROYECTOS = "PROYECTOS"

try{
    //parametros del reporte
    String fechainicial = ""
    params.fechainicial == null ? null : (fechainicial = params.fechainicial.toString())
    
    String fechafinal = ""
    params.fechafinal == null ? null : (fechafinal = params.fechafinal.toString())
    
    String cond1 = ""
    String cond2 = ""
    
    if(fechainicial != ""){
        cond1 = "AND FECHADOCUMENTO >=  TO_DATE('${fechainicial}','DD/MM/YYYY')"
    }
    
    if(fechafinal != ""){
        cond2 = "AND FECHADOCUMENTO <=  TO_DATE('${fechafinal}','DD/MM/YYYY')"
    }
    
    String query = """SELECT SEQ, WORKID
                      FROM Z_ADB_GAB248
                      WHERE ROWSEQNUM = 1
					  ${cond1}
					  ${cond2}
                   """
    
    log.error("Query: " + query)
    
    def WF = sql.runSQLFast(query, false, false, 0).rows
    
    log.error("WF ${WF}")
    
    //crea un libro con sus pestanas
    String[] secciones = [VALORES, REVISOR, APROBADOR, SECCIONB, SECCIONC, SECCIONCGASTOS, SECCIONCPROYECTOS]
    
    def archivoexcel = xlsx.createSpreadsheet(secciones)
    
    //Escribe los titulos de cada pestaña en el excel
    List<String> TVALORES = ["Fecha de Documento","Año de Vigencia","Centro Logístico","Administrador Inventario",
                             "Area de negocio","Gerencia","Solicitante","Rol Solicitante","Fecha de Informe",
                             "Fecha de Corte de la Información","Equipo Técnico Designado","Tipo de Materiales",
                             "Tipo","PEP Origen","PEP Transversal","Líder Proyecto","Gerente Proyecto","Nombre Proyecto",
                             "Estado Proyecto","Adicional","Adicionales"] 
    archivoexcel.getWorksheetByName(VALORES).writeRow(TVALORES,0)
    
    List<String> TREVISOR = ["Fecha de Documento","Usuario", "Rol"] 
    archivoexcel.getWorksheetByName(REVISOR).writeRow(TREVISOR,0)  
    
    List<String> TAPROBADOR = ["Fecha de Documento","Usuario", "Rol"] 
    archivoexcel.getWorksheetByName(APROBADOR).writeRow(TAPROBADOR,0) 
    
    List<String> TSECCIONB = ["Fecha de Documento","Código Material","Descripción","Clasif. ABC","Estado Material","Especialidad",
                              "Motivo Análisis","Cant. SAP","Valor SAP","Concepto Final","Cant.","Valor","Fecha Consumo","Motivo Uso",
                              "Criterio","Explicación Técnica"] 
    archivoexcel.getWorksheetByName(SECCIONB).writeRow(TSECCIONB,0) 
    
    List<String> TSECCIONC = ["Fecha de Documento","Cantidad Códigos Analizados","Valor Analizado","Cantidad Códigos Mantener",
                              "Valor Total Mantener","Cantidad Códigos Transferir","Valor Total Transferir",
                              "Cantidad Códigos Dar de Baja","Valor Total Dar de Baja"] 
    archivoexcel.getWorksheetByName(SECCIONC).writeRow(TSECCIONC,0)
    /*
    List<String> TSECCIONCGASTOS = ["Fecha de Documento","Año","Material a mantener / Compromiso de utilización por año"] 
    archivoexcel.getWorksheetByName(SECCIONCGASTOS).writeRow(TSECCIONCGASTOS,0)
    */
    List<String> TSECCIONCGASTOS = ["Fecha de Documento","Año 1","Material a mantener","Año 2","Material a mantener","Año 3",
                                    "Material a mantener","Año 4","Material a mantener","Año 5 ","Material a mantener",
                                    "Año 6","Material a mantener"] 
    archivoexcel.getWorksheetByName(SECCIONCGASTOS).writeRow(TSECCIONCGASTOS,0)
    
    List<String> TSECCIONCPROYECTOS = ["Fecha de Documento","Vr. Total Materiales del Proyecto","% Sobrantes vs. Total materiales"] 
    archivoexcel.getWorksheetByName(SECCIONCPROYECTOS).writeRow(TSECCIONCPROYECTOS,0)
    
    //Declaramos los contadores
    int count = 2
    int count2 = 2
    int count3 = 2
    int count4 = 2
    int count5 = 2
    int count6 = 2
    
    //Patrones para formatear capos fecha
    SimpleDateFormat sdf=new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy")
    SimpleDateFormat sdf2=new SimpleDateFormat("dd/MM/yyyy")
    
    //String para quitar tags html de campos multiline
    String strRegEx = "<[^>]*>"
    
    //Inicio iteración para llenar los datos en las pestañas del excel
    
    for(int i = 0; i<WF.size(); i++){
    //Escribe datos Pestaña VALORES UNICOS
        
        List<String> Encabezado = new ArrayList<String>()
        
        //se obtiene el workflow
        form = workflow.getWorkflowStatus(WF.get(i).get("WORKID") as long)?.getFormData("Form")
        
        //fecha de documento
        Encabezado.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
        
        //año vigencia
        Encabezado.add(form.AnoVigencia.value.toString())
        
        //centro logístico Código - Nombre
        def query10 = """SELECT CONCAT(CONCAT(Centro,'-'),NombreCentro) AS CENTROLOGISTICO
						 FROM Z_EC_CENTROLOG
						 WHERE Centro = '${form.CentroLogistico.value.toString()}'
					  """
        query101=sql.runSQLFast(query10, false, false, 1).rows
        
        Encabezado.add(query101.get(0).get("CENTROLOGISTICO"))
        
        //adin
        Encabezado.add(form.AdmonInventario.value.toString())
        
        //área de negocio
        Encabezado.add(form.AreaNegocio.value.toString())
        
        //gerencia
        Encabezado.add(form.Gerencia.value.toString())
        
        //nombre del solicitante
        Encabezado.add(users.getUserById(form.Solicitante.value as long)?.displayName)
                
        //rol del solicitante
        def query11 = """SELECT DESCRIPCIONLARGA
                            FROM Z_ADB_Parametros
                            WHERE Tipo = 'Rol' 
                            AND Sigla ='${form.RolSolicitante.value.toString()}'
                        """      
        query111=sql.runSQLFast(query11, false, false, 1).rows
        
        Encabezado.add(query111.get(0).get("DESCRIPCIONLARGA"))
        
        //fecha del informe
        Encabezado.add((form.FechaInforme.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaInforme.value.toString()))))
        
        //fecha de corte de la información
        Encabezado.add((form.FechaCorteInfo.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaCorteInfo.value.toString()))))
        
        //equipo técnico designado
        Encabezado.add(users.getUserById(form.EquipoDesignado.value as long)?.displayName)
         
        //tipo de material
        Encabezado.add(form.Tipo.value.toString())
        
        //tipo
        Encabezado.add((form.Tipo2.value)==null ? "No Aplica" :form.Tipo2.value)
        
        //pep origen
        Encabezado.add((form.PEPOrigen.value)==null ? "No Aplica" :(form.PEPOrigen.value))
        
        //pep transversal
        Encabezado.add((form.PEPTransversal.value)==null ? "No Aplica" :(form.PEPTransversal.value))
        
        //lider de proyecto
        Encabezado.add((form.LiderProyecto.value)==null ? "No Aplica" :(form.LiderProyecto.value))
        
        //gerente de proyecto
        Encabezado.add((form.GerenteProyecto.value)==null ? "No Aplica" :(form.GerenteProyecto.value))
        
        //nombre del proyecto
        Encabezado.add((form.NombreProyecto.value)==null ? "No Aplica" :(form.NombreProyecto.value))
        
        //estado del proyecto
        Encabezado.add((form.EstadoProyecto.value)==null ? "No Aplica" :(form.EstadoProyecto.value))
        
        //adicional
        Encabezado.add((form.Adicional.value.toString())== "true" ? "SI":"NO")
        
        //adicionales
        Encabezado.add(form.Adicionales.value.toString().replaceAll(strRegEx, ""))
        
     	archivoexcel.getWorksheetByName(VALORES).writeRow(Encabezado,i+2)
        
        //Escribe datos Pestaña REVISOR
        for(int j = 0; j<form.Revisor.size(); j++)
        {
            List<String> Revisores = new ArrayList<String>()
            
            //fecha de documento
            Revisores.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
            //nombre del revisor
            Revisores.add(users.getUserById(form.Revisor[j].Usuario.value as long)?.displayName)
            
            //rol del revisor
            def query13 = """SELECT DESCRIPCIONLARGA
                            FROM Z_ADB_Parametros
                            WHERE Tipo = 'Rol' 
                            AND Sigla ='${form.Revisor[j].Rol.value.toString()}'
                        """      
            query131=sql.runSQLFast(query13, false, false, 1).rows
            
            Revisores.add(query131.get(0).get("DESCRIPCIONLARGA"))
            
            archivoexcel.getWorksheetByName(REVISOR).writeRow(Revisores,count++)
        }
        
        //Escribe datos Pestaña APROBADOR
        for(int k = 0; k<form.Aprobador.size(); k++)
        {
            List<String> Aprobadores = new ArrayList<String>()
            
            //fecha del documento
            Aprobadores.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
            //nombre del aprobador
            Aprobadores.add(users.getUserById(form.Aprobador[k].Usuario.value as long)?.displayName)
            
            //rol del aprobador
            def query14 = """SELECT DESCRIPCIONLARGA
                            FROM Z_ADB_Parametros
                            WHERE Tipo = 'Rol' 
                            AND Sigla ='${form.Aprobador[k].Rol.value.toString()}'
                        """      
            query141=sql.runSQLFast(query14, false, false, 1).rows
            
            Aprobadores.add(query141.get(0).get("DESCRIPCIONLARGA"))
                        
            archivoexcel.getWorksheetByName(APROBADOR).writeRow(Aprobadores,count2++)
        }
        
        //Escribe datos Pestaña SECCIONB
        
        for(int l = 0; l<form.SeccionB.size(); l++)
        {
            List<String> SeccionB = new ArrayList<String>()
            
            //fecha de documento
            SeccionB.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
            //Código Material
            SeccionB.add(form.SeccionB[l].CodigoMaterial.toString())
            
            //Descripción
            SeccionB.add(form.SeccionB[l].Descripcion.toString())
            
            //Clasif. ABC
            SeccionB.add(form.SeccionB[l].Clasificacion.toString())
            
            //Estado Material
            SeccionB.add(form.SeccionB[l].EstadoMaterial.toString())
            
            //Especialidad
            SeccionB.add(form.SeccionB[l].Especialidad.toString())
            
            //Motivo Análisis
            SeccionB.add(form.SeccionB[l].MotivoAnalisis.toString())
            
            //Cant. SAP
            SeccionB.add(form.SeccionB[l].CantidadSAP.toString())
            
            //Valor SAP
            SeccionB.add(form.SeccionB[l].ValorTotalSAP.toString())
            
            //Concepto Final
            SeccionB.add(form.SeccionB[l].ConceptoFinal.toString())
            
            //Cant.
            SeccionB.add(form.SeccionB[l].Cantidad.toString())
            
            //Valor
            SeccionB.add(form.SeccionB[l].Valor.toString())
            
            //Fecha Consumo
            SeccionB.add((form.SeccionB[l].FechaConsumo.value)==null ? "No Aplica": form.SeccionB[l].FechaConsumo.value)
            
            //Motivo Uso
            SeccionB.add((form.SeccionB[l].MotivoUso.value)==null ? "No Aplica": form.SeccionB[l].MotivoUso.value)
            
            //Criterio
            SeccionB.add(form.SeccionB[l].Criterio.toString())
            
            //Explicación Técnica
            SeccionB.add(form.SeccionB[l].ExpTecnica.toString())
            
            archivoexcel.getWorksheetByName(SECCIONB).writeRow(SeccionB,count3++)
        }
        
        //Escribe datos Pestaña SECCIONC
        List<String> SeccionC = new ArrayList<String>()
        
        //fecha del documento
		SeccionC.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
        
        //Cantidad Códigos Analizados
        SeccionC.add(form.CantidadCodigos.value.toString())
        
        //Valor Analizado
        SeccionC.add(form.ValorAnalizado.value.toString())
        
        //Cantidad Códigos Mantener
        SeccionC.add(form.CantidadMantener.value.toString())
        
        //Valor Total Mantener
        SeccionC.add(form.ValorMantener.value.toString())
        
		//Cantidad Códigos Transferir
        SeccionC.add(form.CantidadTransferir.value.toString())
        
        //Valor Total Transferir
        SeccionC.add(form.ValorTransferir.value.toString())
        
        //Cantidad Códigos Dar de Baja
        SeccionC.add(form.CantidadDarBaja.value.toString())
        
		//Valor Total Dar de Baja
        SeccionC.add(form.ValorDarBaja.value.toString())

        archivoexcel.getWorksheetByName(SECCIONC).writeRow(SeccionC,count4++)
        
        //Escribe datos Pestaña SECCIONCGASTOS
        List<String> SeccionCGastos = new ArrayList<String>()
        
        //fecha del documento
		SeccionCGastos.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
        
        //año 1
        SeccionC.add(form.Anio1Txt.value.toString())
        SeccionC.add(form.Anio1.value.toString())
        
        //año 2
        SeccionC.add(form.Anio2Txt.value.toString())
        SeccionC.add(form.Anio2.value.toString())
        
        //año 3
        SeccionC.add(form.Anio3Txt.value.toString())
        SeccionC.add(form.Anio3.value.toString())
        
        //año 4
        SeccionC.add(form.Anio4Txt.value.toString())
        SeccionC.add(form.Anio4.value.toString())
        
        //año 5
        SeccionC.add(form.Anio5Txt.value.toString())
        SeccionC.add(form.Anio5.value.toString())
        
        //año 6
        SeccionC.add(form.Anio6Txt.value.toString())
        SeccionC.add(form.Anio6.value.toString())
        
        /*
        //se capturan los años
        String[] anios = [form.Anio1Txt.value.toString(),form.Anio2Txt.value.toString(),form.Anio3Txt.value.toString(),
                          form.Anio4Txt.value.toString(),form.Anio5Txt.value.toString(),form.Anio6Txt.value.toString()]
        String[] aniosTotales = [form.Anio1.value.toString(),form.Anio2.value.toString(),form.Anio3.value.toString(),
                          		 form.Anio4.value.toString(),form.Anio5.value.toString(),form.Anio6.value.toString()]
        
        //se recorren los años        
        for(int m = 0;m < anios.size();m++)
        {
            if(anios[m] != "N/A" || anios[m] != null || anios[m] != "")
            {
                log.error("if: ${anios[m]} - ${aniosTotales[m]}")
                //fecha del documento
				SeccionCGastos.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
                
                //año
                SeccionCGastos.add(anios[m])
                
                //Material a mantener / Compromiso de utilización por año
                SeccionCGastos.add(aniosTotales[m])
            }
        }
        
        */
        archivoexcel.getWorksheetByName(SECCIONCGASTOS).writeRow(SeccionCGastos,count5++)
        
        //Escribe datos Pestaña SECCIONCPROYECTOS
        List<String> SeccionCProyectos = new ArrayList<String>()
        
        //fecha del documento
		SeccionCProyectos.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
        
        //Vr. Total Materiales del Proyecto
        SeccionCProyectos.add(form.TotalMaterialesProyecto.value.toString())
        
        //% Sobrantes vs. Total materiales
        SeccionCProyectos.add(form.SobrantesVSTotalmateriales.value.toString())
        
        archivoexcel.getWorksheetByName(SECCIONCPROYECTOS).writeRow(SeccionCProyectos,count6++)
		
    }
    response.file(archivoexcel.save(), true)
    
}
catch(e){
    log.error("---ERROR Reporte GAB248: {}",e)
}
