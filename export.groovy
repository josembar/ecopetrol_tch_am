
//return admin.exportXml(self.parent)

def options = admin.newExportOptions
    options.setPermissions(true)
    options.setScope('Sub')
    options.setVersionInfo('Current')
    options.setContent('Base64')
    options.setAttributeInfo(true)
    options.setNodeInfo(true)
    options.setExtUserInfo(true)
return admin.exportXml( self.parent, options )
