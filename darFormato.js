
/*
EIM Developer: José Barrantes
Función para darle formato al campo form.items.cantidad.value y guardarlo como texto
en el campo form.items.cantidadtxt.value
(total con IVA)
*/

function darFormato ()
{
    //variables para extraer los atributos del campo VTPLPMA del template
    //variable para extraer el prefijo que va a llevar el valor, ejemplos: $, €
    //var prefijo = $(".detallerequisicionVTPLPMA input[name^='amcurrency_']").attr("data-prefix");
    var prefijo = $(".itemsCantidad input[name^='amcurrency_']").attr("data-prefix");
    //console.log(`Prefijo: ${prefijo}`)
    
    //variable para extraer el separador de centécimas (decimales) data-cents-separator
    //var separadorCent = $(".detallerequisicionVTPLPMA input[name^='amcurrency_']").attr("data-cents-separator");
    var separadorCent = $(".itemsCantidad input[name^='amcurrency_']").attr("data-cents-separator");
    //console.log(`Separador de centécimas: ${separadorCent}`)
    
    //variable para extraer el separador de miles, ej: 1000 sería 1'000.00 data-thousands-separator
    //var separadorMil = $(".detallerequisicionVTPLPMA input[name^='amcurrency_']").attr("data-thousands-separator");
    var separadorMil = $(".itemsCantidad input[name^='amcurrency_']").attr("data-thousands-separator");
    //console.log(`Separador de miles: ${separadorMil}`)
    
    //variable para extraer el máximo de centécimas data-cents-limit
    //var maximoCent = Number($(".detallerequisicionVTPLPMA input[name^='amcurrency_']").attr("data-cents-limit"));
    var maximoCent = Number($(".itemsCantidad input[name^='amcurrency_']").attr("data-cents-limit"));
    //console.log(`maximoCent: ${maximoCent}`)
    
    //variable para extraer clearOnEmpty data-clear-on-empty
    //var clearonempty = $(".detallerequisicionVTPLPMA input[name^='amcurrency_']").attr("data-clear-on-empty");
    var clearonempty = $(".itemsCantidad input[name^='amcurrency_']").attr("data-clear-on-empty").toLowerCase() == 'true' ? true : false;
    //console.log(`clearonempty: ${clearonempty}`)
    
    //fin de variables para extraer los atributos del campo itemsCantidad del template
    
    //dar formato a itemsCantidad
    for (var i = 0; i < $(".itemsCantidad input[name^='amcurrency_']").size(); i++) 
    {   
        //variable para almacenar la cantidad
        var cantidad = 0;
		cantidad = Number($(".itemsCantidad input[name^='amcurrency_']").eq(i).val().replace(/[^0-9\.-]+/g,""));
        cantidad = isNaN(cantidad)?0:cantidad;
        //console.log(`Cantidad: ${cantidad}`)
        
        //variable para almacenar el equivalente en texto de la variable cantidad
        var cantidadTxt = cantidad.toLocaleString('en-US',{style:'currency',currency:'USD'});
        cantidadTxt = cantidadTxt.replace(/,/g,"'").replace('$','');
        //console.log(`cantidadTxt: ${cantidadTxt}`)
        
        //se le asigna el valor de cantidadAproximadaTxt al campo oculto del Custom HTML del formulario
        $(".am-form-custom-html > .cantidadtxt").eq(i).val(cantidadTxt);
    }
}

$(function(){
    //Cuando cargue la pagina calcula los totales
    darFormato();
});