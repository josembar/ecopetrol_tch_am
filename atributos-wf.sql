
select workflows.work_workid,
subwfs.subwork_title,
workflows.work_dateinitiated,
workflows.work_datecompleted,
workflows.work_ownerid,
workflows.work_managerid,
workflows.work_status,
attributos.wf_attrid,
attributos.wf_valstr
from wwork workflows
inner join wfattrdata attributos
on workflows.work_workid = attributos.wf_id
inner join wsubwork subwfs
on workflows.work_workid = subwfs.subwork_workid
where attributos.wf_valstr = 'FNX000222'