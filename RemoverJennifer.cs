/*
████████╗███████╗ ██████╗██╗  ██╗███████╗██████╗  ██████╗ ███████╗    ████████╗███████╗ █████╗ ███╗   ███╗
╚══██╔══╝██╔════╝██╔════╝██║  ██║██╔════╝██╔══██╗██╔════╝ ██╔════╝    ╚══██╔══╝██╔════╝██╔══██╗████╗ ████║
   ██║   █████╗  ██║     ███████║█████╗  ██║  ██║██║  ███╗█████╗         ██║   █████╗  ███████║██╔████╔██║
   ██║   ██╔══╝  ██║     ██╔══██║██╔══╝  ██║  ██║██║   ██║██╔══╝         ██║   ██╔══╝  ██╔══██║██║╚██╔╝██║
   ██║   ███████╗╚██████╗██║  ██║███████╗██████╔╝╚██████╔╝███████╗       ██║   ███████╗██║  ██║██║ ╚═╝ ██║
   ╚═╝   ╚══════╝ ╚═════╝╚═╝  ╚═╝╚══════╝╚═════╝  ╚═════╝ ╚══════╝       ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝     ╚═╝
                                            ╦╔╗╔╔═╗╔═╗╦╦═╗╦╔╗╔╔═╗  ╔╦╗╦═╗╦ ╦╔═╗╔╦╗  ╔═╗╦  ╔═╗╔╗ ╔═╗╦  ╦ ╦ ╦
                                            ║║║║╚═╗╠═╝║╠╦╝║║║║║ ╦   ║ ╠╦╝║ ║╚═╗ ║   ║ ╦║  ║ ║╠╩╗╠═╣║  ║ ╚╦╝
                                            ╩╝╚╝╚═╝╩  ╩╩╚═╩╝╚╝╚═╝   ╩ ╩╚═╚═╝╚═╝ ╩   ╚═╝╩═╝╚═╝╚═╝╩ ╩╩═╝╩═╝╩ 
*/
/***** Remover Profesional de Convenios BEGIN - DGC 19/02/18*****/
    Date inicio = new Date();
	app = docman.getScriptByNickname('am_convenios_config').getCSVars()?.app
	//SELECCCIONAR TODOS LOS BINDER COMENZANDO EN LA CARPETA 01.CONVENIOS COMO PADRE
	String querydtreecore = """SELECT dt.dataid
                            FROM dtreecore dt
                            WHERE subtype='31066' AND deleted=0 
							start with dt.dataid = ${app.constants.parentConvenios}
                            connect by abs(dt.parentid) = prior dt.dataid"""
	
    def dtdataid = sql.runSQLFast(querydtreecore,false,false,1).rows
	out<<"Numero total de Binders encontrados: ${dtdataid.size()}"
    def rightid = [[:]]
	for(int i=0;i<dtdataid.size();i++){
        //SELECCCIONAR TODOS LOS RIGHTID CON NAME IDBINDER+PROFESIONALDECONVENIOS
        rightid<<['binderid':dtdataid.get(i).get("DATAID"), 
                  'rightid':sql.runSQLFast("""SELECT K.ID
							  	   			  FROM KUAF K
								  			  WHERE K.NAME = '${dtdataid.get(i).get("DATAID")}_Profesional de Convenios'""",
                                              false,false,1).rows.get(0).get("ID")]
    }
	//QUITAR EL USUARIO -- Jennifer Elena Calderon Granados
	long User = users.getUserByLoginName("e0992862@red.ecopetrol.com.co").ID
	for(int j=1;j<rightid.size();j++){
        try{
            users.removeMemberFromGroup(users.getGroupById(Long.valueOf(rightid.get(j).get('rightid'))),users.getMemberById(User))
            out<<"Usuario removido - BINDERID:${rightid.get(j).get('binderid')} - RIGHTID:${rightid.get(j).get('rightid')}<br>"
        }
        catch(e){
            out<<"Error - BINDERID:${rightid.get(j).get('binderid')} - RIGHTID:${rightid.get(j).get('rightid')}<br>"
        }
    }
	out<<"Hora Inicio = ${inicio.format('h:mm:ss a')} - Hora Fin = ${new Date().format('h:mm:ss a')}"
/***** Remover Profesional de Convenios END - DGC 19/02/18*****/