//obtiene variables

//Developer: DEP

log.error("Inicio CS Mover adjuntos")

//variable para capturar la carpeta de adjuntos del workflow
def adjuntosFolder

//variable para obtener los atributos del workflow
//def atributos

//variable para capturar el estado del workflow
def workflowStatus

//variable para capturar el formulario en una tarea del workflow
def formulario

//variable para capturar la tarea de un workflow
def tareaW

//query comentareado por José Barrantes 04/01/2019
/*
def queryCarpeta = """SELECT DESCRIPCIONLARGA
					  FROM Z_LI_PARAMETROS
                      WHERE Activo = 1 AND
					  TIPO = 'CarpetaDestinoGAB40'
                   """
*/
    
//obtiene instancia del WF en ejecucion
try{
    
    //llamar archivo de constantes
    //app = docman.getScriptByNickname("am_logisticainversa_config").getCSVars()?.app
    
     //obtener el estado del workflow
    workflowStatus = workflow.getWorkflowStatus(workID,subWorkID)
               
    //obtener los atributos del workflow
    atributos = workflowStatus.getAttributes()
    
    //obtener nombre del formulario
    def nombreFormulario = atributos.data.NombreFormulario
    log.error("nombreFormulario: ${nombreFormulario}")
    
    //obtener la carpeta de adjuntos del workflow
    adjuntosFolder = workflowStatus.getAttachmentsFolder()
    
    //obtener la tarea del workflow
    tareaWF = workflow.getWorkFlowTask(workID,subWorkID,taskID)
        
    //obtener el formación asociada al paso del workflow
    formulario = forms.getWorkFlowForm(tareaWF,nombreFormulario as String)
    
	
}catch(e){
    log.error("Error consultando informacion del WF: ${workID}", e)
}

//código añadido por José Barrantes
//obtener los adjuntos del wf y copiarlos al binder correspondiente
try
{
    //llamar archivo de constantes
    app = docman.getScriptByNickname("am_logisticainversa_config").getCSVars()?.app
    
    //variable para obtener el subtype del padre del formulario
    def subTipoPadre = asCSNode(formulario.binderid.value as long).parent.subtype
    log.error("padre: ${asCSNode(formulario.binderid.value as long).parent}")
    log.error("subtipo: ${subTipoPadre}")
    
    //se obtiene el dataid del binder. El dataid está guardado en el campo binderid del formulario
    def idBinder = formulario.binderid.value
    log.error("el binder es ${idBinder}")
    
    //variable para obtener el binder como nodo
    def binder
        
	//se ejecuta query para obtener el nombre de la carpeta en donde deben quedar almacenados los adjuntos del wf    
    //def carpeta = sql.runSQLFast(queryCarpeta,false,false,0).rows.collect{it.DESCRIPCIONLARGA}.get(0)
    
    /*
	Variable para almacenar el nombre de la carpeta donde quedarán almacenados los adjuntos del wf
	Este nombre cambia para el binder principal y los demás binders
	*/
    def carpeta
    
    //si el binder está dentro de otro binder
    if (subTipoPadre == 310660)
    {
        carpeta = app.constants.carpetaAsignacion
        log.error("if: la carpeta es: ${carpeta}")
        binder = asCSNode(idBinder as long)
        log.error("if: el binder es: ${binder}")
    }
    
    //si el binder está dentro de una de las carpetas correspondientes al tipo de solicitud
    else if (subTipoPadre == 0)
    {	
        carpeta = app.constants.documentosSoporte
        log.error("else if: la carpeta es ${carpeta}")
        binder = asCSNode(-(idBinder as long))
        log.error("else if: el binder es: ${binder}")
    }
    
    //log.error("la carpeta es ${carpeta}")
    
    //se obtiene la carpeta 01 Documentación Soporte que está dentro del binder 
    //def idCarpeta = docman.getNodeByName(asCSNode(idBinder as long), carpeta)
    //def carpetaDestino = docman.getNodeByName(asCSNode(-(idBinder as long)), carpeta as String)
    def carpetaDestino = docman.getNodeByName(binder,carpeta as String)
    
    log.error("La carpetaDestino es ${carpetaDestino}")
        
    //variable para obtener los documentos adjuntos alojados en la carpeta de adjuntos del wf
    def adjuntosWF = docman.getChildrenFast(adjuntosFolder)
    log.error("Los archivos son: ${adjuntosWF}")
    
    def oldDoc
    
    //se crea ciclo for para ir copiando uno a uno los adjuntos del wf a la carpeta carpetaDestino
    for(int i = 0; i < adjuntosWF.size(); i++)
    {
        oldDoc = docman.getNodeByName(carpetaDestino,adjuntosWF.get(i).name as String)
        log.error("oldDoc: ${oldDoc}")
        if (oldDoc)
        {
            log.error("Dentro de if...")
            docman.deleteNode(oldDoc)
            def docNuevo = docman.copyNode(asCSNode(adjuntosWF.get(i).getID() as long),carpetaDestino)
        }
        
        else
        {
            log.error("Dentro de else...")
            log.error("hijos get(i): ${adjuntosWF.get(i).name}")
        	log.error("hijos: ${asCSNode(adjuntosWF.get(i).getID() as long)}")
        	//se copian los adjuntos del wf a la carpeta dentro del binder
       		def newDoc = docman.copyNode(asCSNode(adjuntosWF.get(i).getID() as long),carpetaDestino)
        	//def newDoc = docman.copyNode(asCSNode(adjuntosWF.get(i).getID() as long),carpetaDestino as CSNode)
        	//def newDoc = docman.copyNode(docman.getNode(adjuntosWF.get(i).getID() as long),carpetaDestino)
    	}
	}
        
    log.error("Adjuntos 01 Documentación Soporte ${docman.getChildrenFast(carpetaDestino)}")
}
catch(e)
{
    log.error("Error copiando adjuntos de WF ${workID} en el binder destino", e)
}
//fin de código añadido por José Barrantes

log.error("Fin CS Mover adjuntos")





