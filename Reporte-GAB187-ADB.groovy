/*
████████╗███████╗ ██████╗██╗  ██╗███████╗██████╗  ██████╗ ███████╗    ████████╗███████╗ █████╗ ███╗   ███╗
╚══██╔══╝██╔════╝██╔════╝██║  ██║██╔════╝██╔══██╗██╔════╝ ██╔════╝    ╚══██╔══╝██╔════╝██╔══██╗████╗ ████║
   ██║   █████╗  ██║     ███████║█████╗  ██║  ██║██║  ███╗█████╗         ██║   █████╗  ███████║██╔████╔██║
   ██║   ██╔══╝  ██║     ██╔══██║██╔══╝  ██║  ██║██║   ██║██╔══╝         ██║   ██╔══╝  ██╔══██║██║╚██╔╝██║
   ██║   ███████╗╚██████╗██║  ██║███████╗██████╔╝╚██████╔╝███████╗       ██║   ███████╗██║  ██║██║ ╚═╝ ██║
   ╚═╝   ╚══════╝ ╚═════╝╚═╝  ╚═╝╚══════╝╚═════╝  ╚═════╝ ╚══════╝       ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝     ╚═╝
                                            ╦╔╗╔╔═╗╔═╗╦╦═╗╦╔╗╔╔═╗  ╔╦╗╦═╗╦ ╦╔═╗╔╦╗  ╔═╗╦  ╔═╗╔╗ ╔═╗╦  ╦ ╦ ╦
                                            ║║║║╚═╗╠═╝║╠╦╝║║║║║ ╦   ║ ╠╦╝║ ║╚═╗ ║   ║ ╦║  ║ ║╠╩╗╠═╣║  ║ ╚╦╝
                                            ╩╝╚╝╚═╝╩  ╩╩╚═╩╝╚╝╚═╝   ╩ ╩╚═╚═╝╚═╝ ╩   ╚═╝╩═╝╚═╝╚═╝╩ ╩╩═╝╩═╝╩ 
*/
/***** Reporte Excel GAB187- José Barrantes 28/03/2019 BEGIN *****/


import java.text.SimpleDateFormat
import java.util.Date

//Nombre pestañas
VALORES = "VALORES_UNICOS"
MATBAJA = "MATERIAL DAR BAJA"

try{
    //parametros del reporte
    String fechainicial = ""
    params.fechainicial == null ? null : (fechainicial = params.fechainicial.toString())
    
    String fechafinal = ""
    params.fechafinal == null ? null : (fechafinal = params.fechafinal.toString())
    
    String cond1 = ""
    String cond2 = ""
    
    if(fechainicial != ""){
        cond1 = "AND FECHADOCUMENTO >=  TO_DATE('${fechainicial}','DD/MM/YYYY')"
    }
    
    if(fechafinal != ""){
        cond2 = "AND FECHADOCUMENTO <=  TO_DATE('${fechafinal}','DD/MM/YYYY')"
    }
    
    String query = """SELECT SEQ, WORKID
                      FROM Z_ADB_GAB187
                      WHERE ROWSEQNUM = 1
					  ${cond1}
					  ${cond2}
                   """
    
    log.error("Query: " + query)
    
    def WF = sql.runSQLFast(query, false, false, 0).rows
    
    log.error("WF ${WF}")
    
    //crea un libro con sus pestanas
    String[] secciones = [VALORES, MATBAJA]
    
    def archivoexcel = xlsx.createSpreadsheet(secciones)
    
    //Escribe los titulos de cada pestaña en el excel
    List<String> TVALORES = ["Fecha de Documento","Área de Negocio","Gerencia","Gerente","Solicitante","Rol del Solicitante",
                             "Tipo de Baja","Centro Logístico","Adicional","Adicionales"] 
    archivoexcel.getWorksheetByName(VALORES).writeRow(TVALORES,0) 
    
    List<String> TMATBAJA = ["Fecha de Documento","Código Material","Descripción","UM","Cantidad a dar de baja",
                             "Valor Unitario libros a la fecha","Valor Total libros a la fecha","Cant. Subasta",
                             "Vr. Unitario Subasta","Vr. Total Subasta","No. Lote","Diferencia",
                             "Número de reserva/pedido de venta","Observación"] 
    archivoexcel.getWorksheetByName(MATBAJA).writeRow(TMATBAJA,0)
    
    //Declaramos los contadores
    int count = 2
    
    //Patrones para formatear capos fecha
    SimpleDateFormat sdf=new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy")
    SimpleDateFormat sdf2=new SimpleDateFormat("dd/MM/yyyy")
    
    //String para quitar tags html de campos multiline
    String strRegEx = "<[^>]*>"
    
    //Inicio iteración para llenar los datos en las pestañas del excel
    
    for(int i = 0; i<WF.size(); i++){
    //Escribe datos Pestaña VALORES UNICOS
        
        List<String> Encabezado = new ArrayList<String>()
        
        //se obtiene el workflow
        form = workflow.getWorkflowStatus(WF.get(i).get("WORKID") as long)?.getFormData("Form")
        
        //fecha de documento
        Encabezado.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
        
        //área de negocio
        def query9 = """SELECT NOMBRE
						FROM Z_CVN_DEPENDENCIAS3	
						WHERE ACTIVO = 1
						AND SIGLA = '${form.AreaNegocio.value}'
					 """
        query91 = sql.runSQLFast(query9, false, false, 1).rows
        
        Encabezado.add(query91.get(0).get("NOMBRE"))
        
        //Gerencia
        Encabezado.add(form.Gerencia.value.toString())
        
        //Gerente
        Encabezado.add(form.Gerente.value.toString())
        
        //nombre del solicitante
        Encabezado.add(users.getUserById(form.Solicitante.value as long)?.displayName)
                
        //rol del solicitante
        def query11 = """SELECT DESCRIPCIONLARGA
                            FROM Z_ADB_Parametros
                            WHERE Tipo = 'Rol' 
                            AND Sigla ='${form.RolSolicitante.value.toString()}'
                        """      
        query111=sql.runSQLFast(query11, false, false, 1).rows
        
        Encabezado.add(query111.get(0).get("DESCRIPCIONLARGA"))
        
        //tipo de baja
        Encabezado.add(form.TipoBaja.value.toString())
        
        //centro logístico Código - Nombre
        def query10 = """SELECT CONCAT(CONCAT(Centro,'-'),NombreCentro) AS CENTROLOGISTICO
						 FROM Z_EC_CENTROLOG
						 WHERE Centro = '${form.CentroLogistico.value.toString()}'
					  """
        query101=sql.runSQLFast(query10, false, false, 1).rows
        
        Encabezado.add(query101.get(0).get("CENTROLOGISTICO"))
        
        //adicional
        Encabezado.add((form.Adicional.value.toString())== "true" ? "SI":"NO")
        
        //adicionales
        Encabezado.add(form.Adicionales.value.toString().replaceAll(strRegEx, ""))
        
     	archivoexcel.getWorksheetByName(VALORES).writeRow(Encabezado,i+2)
        
        //Escribe datos Pestaña MATBAJA
        
        for(int l = 0; l<form.MatBaja.size(); l++)
        {
            List<String> MatBaja = new ArrayList<String>()
            
            //fecha de documento
            MatBaja.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
            //Código Material
            MatBaja.add(form.MatBaja[l].CodigoMaterial.toString())
            
            //Descripción
            MatBaja.add(form.MatBaja[l].Descripcion.toString())
            
            //UM
            MatBaja.add(form.MatBaja[l].UnidadMedida.toString())
            
            //Cantidad a dar de baja
            MatBaja.add(form.MatBaja[l].Cantidad.toString())
            
            //Valor Unitario libros a la fecha
            MatBaja.add(form.MatBaja[l].ValorUnitario.toString())
            
            //Valor Total libros a la fecha
            MatBaja.add(form.MatBaja[l].Valor.toString())
            
            //Cant. Subasta
            MatBaja.add(form.MatBaja[l].CantidadSubastar.toString())
            
            //Vr. Unitario Subasta
            MatBaja.add(form.MatBaja[l].VUSubasta.toString())
            
            //Vr. Total Subasta
            MatBaja.add(form.MatBaja[l].VTSubasta.toString())
            
            //No. Lote
            MatBaja.add(form.MatBaja[l].NoLote.toString())
            
            //Diferencia
            MatBaja.add(form.MatBaja[l].Diferencia.toString())
            
            //Número de reserva/pedido de venta
            MatBaja.add((form.MatBaja[l].NumeroReserva.toString())=="" ? "No Aplica" :(form.MatBaja[l].NumeroReserva.toString()))
            
            //Observación
            MatBaja.add(form.MatBaja[l].Observacion.toString())
            
            archivoexcel.getWorksheetByName(MATBAJA).writeRow(MatBaja,count++)
        }
        
    }
    response.file(archivoexcel.save(), true)
    
}
catch(e){
    log.error("---ERROR Reporte GAB187: {}",e)
}
