/*
████████╗███████╗ ██████╗██╗  ██╗███████╗██████╗  ██████╗ ███████╗    ████████╗███████╗ █████╗ ███╗   ███╗
╚══██╔══╝██╔════╝██╔════╝██║  ██║██╔════╝██╔══██╗██╔════╝ ██╔════╝    ╚══██╔══╝██╔════╝██╔══██╗████╗ ████║
   ██║   █████╗  ██║     ███████║█████╗  ██║  ██║██║  ███╗█████╗         ██║   █████╗  ███████║██╔████╔██║
   ██║   ██╔══╝  ██║     ██╔══██║██╔══╝  ██║  ██║██║   ██║██╔══╝         ██║   ██╔══╝  ██╔══██║██║╚██╔╝██║
   ██║   ███████╗╚██████╗██║  ██║███████╗██████╔╝╚██████╔╝███████╗       ██║   ███████╗██║  ██║██║ ╚═╝ ██║
   ╚═╝   ╚══════╝ ╚═════╝╚═╝  ╚═╝╚══════╝╚═════╝  ╚═════╝ ╚══════╝       ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝     ╚═╝
                                            ╦╔╗╔╔═╗╔═╗╦╦═╗╦╔╗╔╔═╗  ╔╦╗╦═╗╦ ╦╔═╗╔╦╗  ╔═╗╦  ╔═╗╔╗ ╔═╗╦  ╦ ╦ ╦
                                            ║║║║╚═╗╠═╝║╠╦╝║║║║║ ╦   ║ ╠╦╝║ ║╚═╗ ║   ║ ╦║  ║ ║╠╩╗╠═╣║  ║ ╚╦╝
                                            ╩╝╚╝╚═╝╩  ╩╩╚═╩╝╚╝╚═╝   ╩ ╩╚═╚═╝╚═╝ ╩   ╚═╝╩═╝╚═╝╚═╝╩ ╩╩═╝╩═╝╩ 
*/
/***** Reporte Excel GAB180- José Barrantes 27/03/2019 BEGIN *****/


import java.text.SimpleDateFormat
import java.util.Date

//Nombre pestañas
VALORES = "VALORES_UNICOS"
REVISOR = "RESPONSABLE BODEGA"
APROBADOR = "APROBADOR"
MATERIALREC = "MATERIAL RECIBIDO"

try{
    //parametros del reporte
    String fechainicial = ""
    params.fechainicial == null ? null : (fechainicial = params.fechainicial.toString())
    
    String fechafinal = ""
    params.fechafinal == null ? null : (fechafinal = params.fechafinal.toString())
    
    String cond1 = ""
    String cond2 = ""
    
    if(fechainicial != ""){
        cond1 = "AND FECHADOCUMENTO >=  TO_DATE('${fechainicial}','DD/MM/YYYY')"
    }
    
    if(fechafinal != ""){
        cond2 = "AND FECHADOCUMENTO <=  TO_DATE('${fechafinal}','DD/MM/YYYY')"
    }
    
    String query = """SELECT SEQ, WORKID
                      FROM Z_ADB_GAB180
                      WHERE ROWSEQNUM = 1
					  ${cond1}
					  ${cond2}
                   """
    
    log.error("Query: " + query)
    
    def WF = sql.runSQLFast(query, false, false, 0).rows
    
    log.error("WF ${WF}")
    
    //crea un libro con sus pestanas
    String[] secciones = [VALORES, REVISOR, APROBADOR, MATERIALREC]
    
    def archivoexcel = xlsx.createSpreadsheet(secciones)
    
    //Escribe los titulos de cada pestaña en el excel
    List<String> TVALORES = ["Fecha de Documento","Año de Vigencia","Fecha de Recibido Real","Centro Logístico",
                             "Orden de Compra","Lugar de Recibo de Material","Tipo de Compra","Nombre del Proyecto",
                             "Máscara","Mensaje de Firma","Recibido Por","Rol Recibido Por","Adicional","Adicionales"] 
    archivoexcel.getWorksheetByName(VALORES).writeRow(TVALORES,0)
    
    List<String> TREVISOR = ["Fecha de Documento","Usuario", "Rol"] 
    archivoexcel.getWorksheetByName(REVISOR).writeRow(TREVISOR,0)  
    
    List<String> TAPROBADOR = ["Fecha de Documento","Usuario", "Rol"] 
    archivoexcel.getWorksheetByName(APROBADOR).writeRow(TAPROBADOR,0) 
    
    List<String> TMATERIALREC = ["Fecha de Documento","Posición OC","Código Material","Cantidad","Unidad de Medida",
                                 "Descripción","Tipo Imputación","Número Reserva","Posición Reserva","¿Requiere PEP Pozo?",
                                 "PEP Transversal","PEP Pozo"] 
    archivoexcel.getWorksheetByName(MATERIALREC).writeRow(TMATERIALREC,0)
    
    //Declaramos los contadores
    int count = 2
    int count2 = 2
    int count3 = 2
    
    //Patrones para formatear capos fecha
    SimpleDateFormat sdf=new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy")
    SimpleDateFormat sdf2=new SimpleDateFormat("dd/MM/yyyy")
    
    //String para quitar tags html de campos multiline
    String strRegEx = "<[^>]*>"
    
    //Inicio iteración para llenar los datos en las pestañas del excel
    
    for(int i = 0; i<WF.size(); i++){
    //Escribe datos Pestaña VALORES UNICOS
        
        List<String> Encabezado = new ArrayList<String>()
        
        //se obtiene el workflow
        form = workflow.getWorkflowStatus(WF.get(i).get("WORKID") as long)?.getFormData("Form")
        
        //fecha de documento
        Encabezado.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
        
        //año vigencia
        Encabezado.add(form.AnoVigencia.value.toString())
        
        //fecha de recibido real
        Encabezado.add((form.FechaRecibo.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaRecibo.value.toString()))))
        
        //centro logístico Código - Nombre
        def query10 = """SELECT CONCAT(CONCAT(Centro,'-'),NombreCentro) AS CENTROLOGISTICO
						 FROM Z_EC_CENTROLOG
						 WHERE Centro = '${form.CentroLogistico.value.toString()}'
					  """
        query101=sql.runSQLFast(query10, false, false, 1).rows
        
        Encabezado.add(query101.get(0).get("CENTROLOGISTICO"))
        
        //orden de compra
        Encabezado.add(form.OrdenCompra.value.toString())
        
        //lugar de recibo del material
        Encabezado.add(form.LugarRecibo.value.toString())
        
        //justificación operativa
        Encabezado.add(form.JustificacionOperativa.value.toString())
        
        //tipo de compra
        Encabezado.add(form.TipoDeCompra.value.toString())
        
        //nombre del proyecto
        Encabezado.add(form.NombreProyecto.value.toString())
        
        //máscara
        Encabezado.add(form.Mascara.value.toString())
        
        //mensaje de firma
        Encabezado.add(form.MensajeFirma.value.toString())
        
        //recibido por
        Encabezado.add(form.RecibidoPor.value.toString())
        //Encabezado.add(users.getUserById(form.Solicitante.value as long)?.displayName)
                
        //rol recibido por
        def query11 = """SELECT DESCRIPCIONLARGA
                            FROM Z_ADB_Parametros
                            WHERE Tipo = 'Rol' 
                            AND Sigla ='${form.RolRecibidoPor.value.toString()}'
                        """      
        query111=sql.runSQLFast(query11, false, false, 1).rows
        
        Encabezado.add(query111.get(0).get("DESCRIPCIONLARGA"))
        
        //adicional
        Encabezado.add((form.Adicional.value.toString())== "true" ? "SI":"NO")
        
        //adicionales
        Encabezado.add(form.Adicionales.value.toString().replaceAll(strRegEx, ""))
        
     	archivoexcel.getWorksheetByName(VALORES).writeRow(Encabezado,i+2)
        
        //Escribe datos Pestaña REVISOR
        for(int j = 0; j<form.Revisor.size(); j++)
        {
            List<String> Revisores = new ArrayList<String>()
            
            //fecha de documento
            Revisores.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
            //nombre del revisor
            Revisores.add(users.getUserById(form.Revisor[j].Usuario.value as long)?.displayName)
            
            //rol del revisor
            def query13 = """SELECT DESCRIPCIONLARGA
                            FROM Z_ADB_Parametros
                            WHERE Tipo = 'Rol' 
                            AND Sigla ='${form.Revisor[j].Rol.value.toString()}'
                        """      
            query131=sql.runSQLFast(query13, false, false, 1).rows
            
            Revisores.add(query131.get(0).get("DESCRIPCIONLARGA"))
            
            archivoexcel.getWorksheetByName(REVISOR).writeRow(Revisores,count++)
        }
        
        //Escribe datos Pestaña APROBADOR
        for(int k = 0; k<form.Aprobador.size(); k++)
        {
            List<String> Aprobadores = new ArrayList<String>()
            
            //fecha del documento
            Aprobadores.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
            //nombre del aprobador
            Aprobadores.add(users.getUserById(form.Aprobador[k].Usuario.value as long)?.displayName)
            
            //rol del aprobador
            def query14 = """SELECT DESCRIPCIONLARGA
                            FROM Z_ADB_Parametros
                            WHERE Tipo = 'Rol' 
                            AND Sigla ='${form.Aprobador[k].Rol.value.toString()}'
                        """      
            query141=sql.runSQLFast(query14, false, false, 1).rows
            
            Aprobadores.add(query141.get(0).get("DESCRIPCIONLARGA"))
                        
            archivoexcel.getWorksheetByName(APROBADOR).writeRow(Aprobadores,count2++)
        }
        
        //Escribe datos Pestaña MATERIALREC
        
        for(int l = 0; l<form.MaterialRec.size(); l++)
        {
            List<String> MaterialRec = new ArrayList<String>()
            
            //fecha de documento
            MaterialRec.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
            //Posición OC
            MaterialRec.add(form.MaterialRec[l].PosOrdenCompra.toString())
            
            //Código Material
            MaterialRec.add(form.MaterialRec[l].CodigoMaterial.toString())
            
            //Cantidad
            MaterialRec.add(form.MaterialRec[l].Cantidad.toString())
            
            //Unidad de medida
            MaterialRec.add(form.MaterialRec[l].UnidadMedida.toString())
            
            //Descripción
            MaterialRec.add(form.MaterialRec[l].DescMaterial.toString())
            
            //Tipo imputación
            MaterialRec.add(form.MaterialRec[l].TipoImputacion.toString())
            
            //Número de reserva
            MaterialRec.add(form.MaterialRec[l].NumeroReserva.toString())
            
            //Posición de reserva
            MaterialRec.add(form.MaterialRec[l].PosicionReserva.toString())
            
            //¿Requiere PEP pozo?
            MaterialRec.add(form.MaterialRec[l].ReqPEPPozo.toString())
            
            //PEP transversar
            MaterialRec.add(form.MaterialRec[l].PEPTransversal.toString())
            
            //PEP pozo
            MaterialRec.add(form.MaterialRec[l].PEPPozo.toString())
            
            archivoexcel.getWorksheetByName(MATERIALREC).writeRow(MaterialRec,count3++)
        }
        
    }
    response.file(archivoexcel.save(), true)
    
}
catch(e){
    log.error("---ERROR Reporte GAB180: {}",e)
}
