

-- actualizar consecutivo
update z_ab_consecutivofn set consecutivo = lpad(to_char(to_number(consecutivo)),3,'0') 
where formulario = 'GAB54 - Retiro de bienes' and consecutivo = 0

--borrar consecutivos
delete from z_ab_consecutivofn where formulario = 'GAB54 - Retiro de bienes'

--insertar consecutivo inicial para un formulario
insert into z_ab_consecutivofn values
('GABxx - Formulario Prueba',
lpad(to_char(to_number(0)),3,'0'), -- el número 3 se puede cambiar de acuerdo a la necesidad
1000,
current_date)

-- encontrar el 'consecutivo' que sigue
SELECT LPAD(TO_CHAR(TO_NUMBER(CONSECUTIVO)+1),3,'0') 
FROM Z_AB_ConsecutivoFN
WHERE FORMULARIO = 'GAB54 - Retiro de bienes'
ORDER BY FECHACREACION DESC
FETCH FIRST ROW ONLY