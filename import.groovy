
/*
def csvol = docman.getNodeByName(self.parent, "export-application.xml").content
admin.importXml(self.parent, csvol.content)
*/

def options = admin.newImportOptions
options.setInheritPermissions(0)
options.setOwner(true)
options.setOwnerID(1000)
options.setCreator(true)
options.setCreatorID(1000)
options.setExtUserInfo(true)
def csvol = docman.getNodeByName(self.parent, "export-.xml").content
admin.importXml(self.parent, options, csvol.content)
