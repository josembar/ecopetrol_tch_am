<!DOCTYPE html>
<html lang="en">
    <head>
        #set( $title = "" )
        #set( $subtitle1 = "" )
        #set( $subtitle2 = "" )
        #set( $format = "" )
        #set( $date = "" )
        #set( $version = "" )
        #set( $logo = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAABDCAYAAADZL0qFAAAABmJLR0QA/wD/AP+gvaeTAAAgAElEQVR4nO2deZhdVZXof2ufc6e6NWWsTAQyhwABTCJkABKRDEyCDCJCd6sgioraoji2UZ6tNrYPFW2xAXnaIoKKipiBKYYMIIQwZA5JCISMVam6VXWnM+z1/rhJSNU9lUpiQhG6ft9X35fsee971tlr77X2PtBNN9100003h4N0dQO6OfK8uP3m9O6W+lMSRtNh6G+aMuK+TSJoV7frWKRbQN5BPLnyisp0xYA766rPuTodPw4Rl2Kwm8bsC/kWb+N3nbD5D61h03biLa1TT1hQ7BaazukWkHcIT6697JKa5OgH0olBsYamRrJZoTUPQ4+Pk4ilESDvb6e+dSkFv2Fvtk1G7dXnn/r8013Y9Lc13QJyjLNp05PJev50v+tUvc8PW2jMrWTt+gbGj/ga9z40n3eNe4ZBgxK4kgAg1DyqFgBVQKGowcjLT39hfRd2421Lt4Acw6ze8vORzd7Kx40xg7LFLeS8N7DqkSsojj2OeDwEZxsS8SurQmiVxxYWmPru5H2XT1z2obe+B29/3K5uQDeHjqrKiq3f+3xj/unvtRQ3G9WA0OZRSjNDRVKALR3kBVUlDKBQUMYMd1m/OXzyLWz+MUW3gLzN+cWTj/RbtGz5yGKx2M8Yp+9xde5xz736vQt8u+GkTGEdoS3sS6sHWHKrKjYUwlDxPKWlJeT1rSFVaZNP78z+6i3oyjFJt4r1Nubsm66/efGql28ThNrKSi6fehI3XDqUYriaptxKAptrkz4IldAvCYPsp1eFtjRj+J6yqyGgsdniijCgzkXEnHv9+c8/8Vb37VihewZ5G3NC/365JatWMKB3b2543zgumzqITP6FSOFQhcBTWlssvlcKa24NEYRcUQkCiw0hlRD61DqkUoJxufK66d3CcSC6BeRthKrKF++9c3hjw87qwXUDg1/N++sJwwYM5Kv/fDZnndqfXS2LaMy9RGBz+3agGppCKtOGRFwQEayFYtEShoqhlCadEDRuQEEMVKSMFxTccR97/3MrurjLb3u6Vay3Edfd/u26vy5evH377gYSsRhjhw7nGx85i1OHDWBX6zPsaF6IHzajtqQ25QuW1RsCBtQ5DOrnEoaCY+so5PJkWhopeCGhVVTBhgoI8bhaV+Mjb7h02Yau7u+xgNPVDejmTZ6f+0R26qUXD9q+u/5d540/me9+4lxGDOpPQ/ZZdrYswgszJVUqVLyCYq3Qs8bQs6fBiJCK96E2OYB4DGJJMK5HPAExV0q7V4Ax5oOfuHT5U13d12OFbhWri/n8f/14xh0P3X+TVZs24mafWf3yOddfNI4bL5lGKlHBrpYl7Gh+imLQCHs9QxQKRUuxoMTjgl8QJKk4kiQeTyOOD66Hus2oNeTzFs9THJflH7/gxd92aYePMboFpIt5adPGhjEnDDn/5Y0b6N+nitkfnsj0CWdiJMbOlsV7hGM3e4VDpLSOcBwh8G1pvWGVdOhgbD3ViaEYkyJUD2MgRAlDVT/A27qFq7q2t8ce3QLyFjP7ydnuE3944wMbd2xrfNeIETtT1ens8nUvZ88+dVD6W9dPYnj/cSjhHuFY2EY49uIYIZkUgrQhm7Mk5Hjyud0Ui63kCgtIJgXjlITJ9x2vudnEWnO68Tufem5d1/T62KVbQN5i7vrp0uqRg4f9z/bdDfzl6V0kYjE+dskgbrh4Gr2rxuIFTexo/hsN2efxgybaCweUZpB4UqgSBzcmVCWr8YpVhOqjQYHAL1r1Q/UDdXJ5Gw/UoyIZfOut7+2xj+nqBvxvY8uD83YP7tv3dgQG16W480uj+MxlF9G78hQK/g62Ns1nV+vTeEEjAK6pIBWrIxXrhyNpwsBg1WDEEIsJlVWGyrRQWelQkTTE4zHQpAnDhGOtgzEQTwi9kvy1i7t+TNK9zfsWcO7nbvz8qtc2fmZnpmlhdapihcJ1F06pGXbj+09kYM8JpGJ9aSluZHvmCZoL67HWw3EqqIj1pyZ1IlXJEQShx5ZdT7MrswHHKeDEfNxYiIhSmRiJDRy8wCcIArzQJwh9/KD0F1pv+UfPXfyurh6HY5FuFestIB/aeedNOPP7v33y0Q8N7JviK/80gjPHTKAifhyhFtjRsoiG1mfJeW8AQio+kB4VJ1NbcTLpxGASTk+amutpblrP7p2NpKp2U1lTWovg+KABYkqzBUYR+6ZapoCIfrOr+n6s0y0gR4lxH/unrxcCv2JnU2aOI9L0/NrV4Wc/cLpz7fRT6FU5HMckaS1uZlfrUjK5VfhhK45JUpUYRo/0KSTcXnhBE63FV8EampqLNDTWk21N4sQTGJsnDGI4JkSFfXaON9mz64XdtXnakoe7YAjeEXQLyFFi0sljV+a94u/nPLPkS3U9HW69/lxGDhqKkTgFfweZ/Cp2514i721DNSDm1lCTHE1VajhhmGN785Pk/e0EYR61hmxzJYVCLWpjAKgIqAvqwB439/aoqu97iTNmSwcJuumUbgE5kkwbP+qD58767SNLnnpo0YrlxREDj+P2my7n3WP64zqGnPc6TblVtBQ3UPTrCW0BRYk7PeiROpl04jiyxddozq+hEOzGWou1LqGXJJd1CAMHxwmIxQLC0MFxHFTNHjeSN42Iqiga/jlO4fprZjy1q0vH5Bine5F+BPn3B+7ps3V7w85tDfVcPHkMZ57cl2SiQM57nUx+Na3FV/GCDFZ99j7QcVNLbfokkrG+tBRepaWwEd/PEYYJrF9B4FVQLMTJ52IEAbiJFtLVLRiJk0gEOLECjhMguEssckvo2M3e6rqtV175YNi1o/HOoHsG+ceR7/7qV2Obi62hl8/fNPX0fowYfDzJxG5ai39jW8sGikHDHg/cYN+JPlVw3QoqkoNwTCWNubU0Z7fgeaB+HzRMEwZxrFXEeKSqmgmDEDUh6qcwKR8E1Mawoqh6y983bumirh6MdxrdAvIP8m933XlrfXbTV888uYohg3yQ12n2trAju5MgbC0JBbbkUWuVMIQwANd1SVX0BhtnV9N6srlWvEINNkxhcDFuiJtoxWqefDEgKBhUHeJxSyzlEYtZBAc0Bqr4Aed19Vi8E+kWkMNk9v13jGzY9fefjxj2/DmD+3l4YYadzU2E5EFDkDfXxaoQhorvQT5nMQ5UVSQJgxT1zRkyzQF+oQeO45BIesQqGrFhlpZsSGuLg+tCLGaIJwMSyZBYPMQIqJaERtVFQzu4C4fjHUv3GuQQuPtPf6p69MXfX1Td49X/M+lkZ0h10iG0gh96qBYxJiAeBzcGxpRcQqDknu4XoaU5pFhU+g1wMZIi29yDTEOaMEgQTwZUVbcgppVMpkA2K7gxIR43xONCPKHE4xY3bnEcUOugYQpsHOP65HOWOuvHpk1bEHTtKL2z6J5BOuG5556L3b3k1+dUVm77Vt9BD5/56TFxCXUoRgzGuAhQ9HK8sX0rmeZmUmlLKiUkkoLZs7lkw9LM0dJs6dvfxXEM+VZDZneMXDZJRbpIurqJ1nyGTKNPLCFUpB1icYjHLLEExGKKcRQRRdXBWge1DiIQhBCGDtuDXF9gaxcP2TuKbgHZwxUP4FwzdnJFknRvE5qBhvjJXlh7oZf+xXkfuTgZN+YEHElSnRpN7/Q4KhMnIOKy6o0f8XrmWQRLQ71S5ZfKcxwlFi/9OwygWFSSKUNVlSEMhWIhTi4bx3V8Eqkmtm+vB7FU1gjJJMRiIW6s5NbuGPbcbWWw1sH6cdTGEXHAWHKtBuMoFVUVQ+gWkCPK/xYBkS/efXGlZ1ODGvOtQ8W3IzxfTvbD5Nh83h26OxPruXNppWT6VDB4WCVxtxZUaS1uoio1kqTbm9Dmqas+m6rkCAAyLfW8uvt+trfMZ3t9hl27Wsk2lx7oZAISScEp2fRKKpYP6cqSRqtWCAKDtYJJ5tmxM4OIT3WNoSJdEhDX2bMRrEpoBRs62LC0KN8rHGIs+RZFTGlN4iunAYu7ZITfobzjBOSx1Zf2iktstLipM+NSObklX3nmHx6v7m9sDXXpSurSpW1W13HoWVVDVUUFjjGkEglqKuMM6gGqAet33U11cgQ1qRNBlWSsD1XJEfi+z8ZtC9iRvx9xioShT6GYpZD3URzCwBAEgg1BtHQ7tA0V37dYWzrhLJRmBMexaOiTz/mk0rrvIoYwLN1hZa1gQ4NVB2zJai7iYhwhZmRRU5M31jFaHXMVIy6iZiLwky4c/nccx7yAPPDAFU7t2OKZSUndkIj1ubIicVwi6fYh7vbANRWIOLjTlN0ZF2stja0teL5fugFELa350vU5WxvqaWjO4CR2E4s1EAQtxJwqXJMktMWSoACrXvsNrbqAylRdySVddmAMpQNKRhAjpdtDAKuKhoLnKUEgNDaE9O5dUovi8QDXDXFjBnHABkKhUCrDcQWD2bdLJeLgGAdjDK6riLH/yfbKW2xFyw2u4SeCA6JYzIwu/CnekRyzAvKJ22eOqm9173liW9+JN04aLBXxOlyTBoTAZskWX8MPM3hBM5vqW/nFnywFz6M5lyW0bY3M1ipe4JMvFuldV2ToIJ9UrC+hzWOtj4iLY5LU764n4y+gtvIErAYU/J2EthWR0tnwVgtGSsKi7F17WAr50iUL+XxIU1NIj54usYSHG/dBHXr1TNLQ4ENBCa0ScwzGERxjMA7EXMW4lnjctvjF2IXXvnfeQoDZT55wz/He8beLSEwQBNP7D4un933/5Pk7u+AneUdyzAnIR25773u3N1Xd19B8Yp/RgwbTo6qanfUF+vbeRs7bRsHfjR+0EAR5wtDHYikGIUtWBkBp1jgQibjFWvCCZloKm3BNFXG3Gqsera2toJW4TiXZ4haKYSOh9RFTskk4MSGWKB13VVu6+zaXVbKtFs+zhCFsfsUnNVaIxUOSKY9ctpKqyiqy+SJBEKBWUWNLs0XMEo8LsdLW7gONO2If/eSV81v3tnX2tFcL9z46+FsIt4qU7sUycfcq4EdH+Wf4X8MxIyC3PPDempUvJ57b2Thq+KlDhlGRTLJ9dwNPr17Bsg3buWZWC37gEdqS/q4qqBVUHXJ508b/qSPSKaFnpeB7QiAFfG89BS9DIlZFEGbZnQ0JNUDERcQgCLrnQlzPFxJxIeaUfM8LBYvnQa5VyecgCEAQ8gXl1Q0Bw0YYKipz+F6SIEgxsH8VDQ0tmJglmaBkMY+D68qzJox/9MpzFr4c1WZ1zffFytdFJC4iGOLfnq3c0e3Be2Q4JgTk6u+d/f6nnqr73dihp0htZSWv7dzBqs0beX3nDppzWYb0Vy6bVlHySwpBcVCr2D1nJHbs9A+qnrPGCvlcbO/2EapZMpnXUeuwyluJX4QTBg/CWg/BoCqEgaAWvLxQkRYQxfMhzEHRU7yiJQwtxoBKSQXLtoa8trnIwEEOlTUt5LNVCGn6DbD4QW6364R/xOhjriYeu3LKgb1xPzxtQeH+x997M/CjPWpW5YSXLvk3+OPsf3TcuzkGBOTKfz/vjvqG0Z8888RhZLKtPP78s7zyxhZacrl96lJoHdTGULtndawh4oCjFlRZ94bXaT0VSZg2NkEhV1KPrC1tKVm1qC1d5RmGQq7QRNZ7jSDM43k+hUJItrV0p6cY8H2hOg3xKti6xe45E24wojiOYBwlFgPHMWQyxY2VVVJb6WjPwE9iTIKqan97IPl7ezf3XLq/VXy2Yia+ND2Vjb+WTHvxwoxTX8rujfvAex6743eLZr1fRKYKgkvyG4+//IEJIN+OV5vVwcaRLdOmze62sB8Gh+Vqok/i0hcjJ9H5k3eoZT9HjBQiJ+FdNHva70w47rLTh49i9eZNLF7xIjubGgnCtovss0+P8ZkrEiX/J7WtiNaraDOW7SJa+cMH7JBdu8N0ECK5Ak4Y4jYXcItFTLhHEfn4++JMGGkIQrC2pDqVBMWWdqMsgNK7Tujdq4KiZ2loaCTT5NO4W4jHHSrTQt2A0kyxa1vJAp5KCW4ML9McPqqBLLA5//kgkVz9p5eX7Vowm2D2bMwpF57xVaOxb6g1jhPzcZwAhLxg11skLlAD9FTFGpGHFHxUT8XI/1x4yrP/CSVD54dOuPiHMSf+yVgshuu6pT9n7zvQ5tQGcyD4eSKTfnL8+J8f3LTalVxxUhzGhDzYda77ByUgurbXwIDwKrCXgBkGWgcYhEaUp1V5OFZM/lJO3ZHttLD2ZW/oMTgI7QewXIowBKgDxA/F39BQF2vwT+OhF/vz20Vr2JVp2qfzA/SulWD8aPeh94yv+F6vWm9DemqflivlwVAV46+r+qgj5garepIiCdnbVyEEyaK6OHDkttTwpr/9y+x/iU8c12CK+SCt+KlEXCutCavJU4lD5V+WBx9xRB1VZcwQO+is8eHY1hbLtq0eTU2B5gpOS11djMEnmEq1Uiz6tNZUUYnozuYMX3j4J8v+8OCDhD/60fBEj9NqelYkw0H1y50Xbrhh2b6H9IFnx4+Kx8znih7ne77W7moUt6mVRP+eao7rp7yw3rQuXWEevfq8IN2ziu/MOuW5BY9tPKPu3CHP7JK9642Zk67qVWXOnHhSRb9U0jiVFRI/dVjV4Amjqk/d0ejvyOaD3MA+iQF5L8jPfabp7z95qP4FG7VSETOXuYsWcP6k0wgP+rK5AKP1qGzH1af4y9I3DvVZYPqUcYi9AmE6MBDoS+m45A5gM/AIVn/H/KVrOi1r5qTZKMmy8FT8u/xxQdPBNumAAqKbSAZ+7VdQvQWId1LWDhX5Unxk070HU7FuHVARNGdnI3yOTlS9wAp3Larlqw/1obVoSCXQWROdL8tLS77/4IO0ebsUV9Veahz9BaW37sG0ZJ3r6kUyrKXjS9VmTmqzuj//DMt54wJS4rN9m08YQm2tQ9/+DtU1Dqs3GxYud9i2m53nnK4Lp70rsI6Rs4BXQR6D8E8Xjn1+WZs6ZkyehujPgJHtq69OK1edayn48Ot5jr1mZvB8rRu75oc3Ll7bJuGsyb9G9er2+SsSwv98bRhXzt7ApWfV0Nga8MqWIjPPqOHuRxrwg/abF/pl5i79LjMmXo3IrzsZwI5YCvpN5i6d12nK884YgePcAUw/iHIVuB91Ps+8p7Z1mGrmpCainoHAHM9ji147iHqAAwiIrq7qFYiZj3BI18UIcoczsummA31iWFem+wVu7HHQMYdS9uptCW56aFDr4LoeJ/zyK483tI8P1tbcqfCxQylzD9a6em1iWPN9kbHtBATgpOOVr11bpGFnQC5vSacdevVxqEgLxggNzfDwIoe/rzZsq5eFfiF4P4//vazNAMyadAvKd+jkhTV6sOV9Z1m+92sXhPWIuZC/LnpTsDsQEIBLz6ph8ctZdjYF/PDTg/jsHVtQLVn0y79MdUQEZE9RcjvzFv8rHW0hzpx4KchvgMShlUsTYi5i7qLoQ2JHU0B0Te+qQPyFwGkHW1C7EmbHRjVHXjWjq6t6BcYsAkYfVsnKhpjHBBmbadw/3FtTe4+IfvhwynyzaPvh+KiW/1cWEyEgyYRy/zc8sjnltW0l+0cqATVVQiKmpPb7uT0flq83O8YO4bqkEz5x0fhluf3K/lfgPw+2kW0eaGE9fnECjy3LAAcUkP3zRQvF/hxBAQFQvs28JV8rC5858QqQ+zj8zaIcKhcyb3H5NxaPkIBENizEv41o4fBA54CsVjhe4FxKemI75Ov+utonYyObFraP8Y25Qw4gHD5OAalMurQiWr42E2FYkOAe4NK9YcHa2kuUToXDV9QI0tEnH8Rg/ltX9Z4vY+o7nrr3UCgK37g7xq0fC5j3iMuC5eWXVLoO1KTh+H6WscO1bswJ9i6JS90jL4370AVjl93H9CnjwH6vgypWIjwG6qByBjAB2j3YyghiyZ8CnX6hdv98BxaOo4BwC7POuo85T63aF3bBlKGE9m46Fg4feB3BRTmO6Jd5BaK/5tx3n9Lh7PwPUtY4f22PKYqNUFNkVRjaS5Jjmvd9T1tfH5QKcy0/VLi+XWIH9JvAtP0Dg7XVs5SOFn36Jyv2y0vCySsG1s4kDHZhWn7PULMVR8p+0Yu9dbWnxUc2vaCKCdfqr6OGr/TJGHnIVfdTMrp+K0D+ldppTsC9Ilp2Ak8hFphgHjA2uo1tefEV+ez134vrV/8puOpD08Oqz9/h/qw5K70RLkc5OQihoRkamg3Pr4N7/+rkmbtkb0sFsXdR/huEqHyKeYvvZH+1ZMbEmYjcT/u3ouoHmT7pO8xfsgLlCaCVMnQKSJQ6uwhYVRaqvNBJ1xeBRljrzXDgJtB+7SJcbPgp4MZ9IaH9OVAVUXYBlVvB/ox5S3cD8N4pg3HsNxA+EpG+PzH3duDaTtp8WJQ9Vv7amj+w39t5T6pGV5yxMmJ32beFVZFgXe2fQS8si0PPiI9q/vu+stfUzNuzQ9G+jPtiozLXzH9pbEU8Obx1QM10Mvk11Lc+Q5W0MCG2Baf0rFiE39vAfCsxpnEFQLi65ivW8O2ozqnIZ+IjmyLdLvy1NUuBM8szKW5MR7VZtEeoWAAExdq96s1fnz+9T+iaay8au+wHTJ2aJOktAU4vy2M5hflLVjBr0ntQHi8vVL7I3MW3RdY3Y9IFCH+J6Ogvmbf4nyPzlPL9FOET5VVxI3OW/FfH+TpQsUTuY87i6Flr1qRhKCug3Q6SsJ45S0obELMmno7K8xG5Q6xewvyl5X0s9eOLCFEzbkBghrVRnY6QitVGL9CV6X7ARWWprPwkSjgApOTV8fXIODX7yiqsrxmGRF4ssCPmOJ8QQXPWxEoW6oDQ5lBCMjbJK0GvJoQHrTWnxUZmrtwrHKWm6aejuyZPdSQcAO7IzDkC5dvSIgSB+b8d5euI89+1fNdFY5f9AIAFCwog0W7nRkoPieoNEbHbqGr6YYeVzFvyCLCkvM16AbPfJheRz1myAfhbWbjy5oxt5VPRmeVnHQoHwLwltyE8GhHjErNR4/kP02ZQQ9c9hyidUMM5ByokPrLpBaBcBxTes68iyzSi9ch7ZMTuZoBEZWveqo8CrpNG9jRlc9jj77GRmSsTJza28UdSJY5IXVSbXBN8/EBtFsET0bsi4+CMA+U9KKSDk32qNaUq5D0RsU/w4MoDG19VH4sI7cXSKaccchuPGuU6Mftf/yhEueUr1ulss0JR7WjNdlRc/dsJg0yJ3I0Tc4m/puasTsqKsHbq+P3+PSVSPkTm7v3n+SNeKT66amg2tIV0wu2NYxLY0ENhQFSF3vrqy0x0oS0yorVct26HcZPfsX7xM+3DLfTSJ0nKNAqdldEx2tH2+E4uOHM0Ib0j4o5n1qRbDlwsp0aGi44HXjykJh4NZkwcDjo1Iqaksk4/ewgEA8ujdTXzF27qtPyqQQtoeaMVqGybndO4eHIVf17ccuiN7pg2AqLooMhUwhcOs/y4buhRI8MaM4IcF9mAMFy5//8DLfxXIdh5c2X8BJat7MHDS7KMGqwnP7DypPiVJ7V9u7rIqVGGYAOvH0zjZOjOHcG66oKqtNGXBaBvejRkO1usRnP+pNOw/GtEjILzPL6cXLrRoYwpKFMOq87I3cSjhOrg0hbt/mFGMDoC5Sbarz9KCUrfJ5HwhOgyJdJbuYwHHwyZOWkZcE67GIcig4GVEbkOm/YzSJ/OXMIPlaKGfYAM0DMqXk5saaOaBVL8eba4+ea1G/szd3Fvlq3ezjMvh2SaK74NbQU1CKWfiXjQrNJYFtgBiuSJ+EHDuDsCOtnNceN/ZObkdk6AWovlNKK3Lxcz76ltzJp89hEeZkCjZqSjxZSStrEfUna9/P60Epo7ADDaKzKdSP1B1y7Ud1BXX46wgLRf2B3xe7LEd1Id1AXscXzcj4vGPLM+72175pdz1+IFPo4pZfvLEnPzJd+e0MY2I6aD9ooe/OetlUinPRvayqjwdhVNBX1v2z/GEy0cisq/lf5lj/yCWkl1nqhLUEQ+zqOLS2sy1Y6esUMwFkoxOth25g51yLT7oewRvwlcNdyrx0cbcgZU1bYP8gvZC1dvXse6La/jB6WlTRjC3KfM8vd9c/wn96YT1R0dVHuQfligEj3Yoeu8erBlHFxF/Ps+i6/IUTBqdfDQdC2tiFzLnMVvbhV32Hfb4+CL1erIYGOO+Li2kVpBdkTOXMJ0Ddl9OBUktHkzgMKOqFdHIM4YoI3F/aLxy+qvum38tIceM0/a/V44ng8PL3Tu6H/Fu3/wrpPMneubX2saUVO+U1vEHfLIS+M/p2ilIJUKKYFKRdOqpuLJ58yAvyxiYGVKsx99wOvfuzKgdzpgWC+PYT0LDKrx9KHV/RrG3Tkstmw/j9vDJED4GnOX/Me+EGt27H816T6U/0bNnYdVSzzcfrgNPEr8hsB8qczmEOpWTNSTICcddMnagSeGf+TvBGsjIFbkaVEtt6IrNn5iZllZ+KEguhSVCCu6PZt2AgJw/xeeW3DB7AnD1m7khY1vSBuLa+9a4tfODD/9arI/Q3VDmaU9Rph0xf7AV1NUeFmUDSpsQ2UDopls0fbs11OG1lWHtWPq8kNznqG+2fDSa5Vs3FnLmi0xsoXSjlDF+e+2owf3oLrKoaEl4PVdBZpzXqdn24EtII+g9vvMXfpKm5jq/itoeaOF9pZkQ0/mLPrHxvmt4QnUfgeVJEZ+Q/sdpRKxSIPc/KVrmTmpHsp28cZw3uQB+1Sxjijtgg0rCxde6zTvYdBOQHShEzGFKHwUoqy+EWk39BgswxrLB0bMU1FOQGrlel3Jf0Qdvnpk9rMbgerbfjH8sy+8XnHTg4tSQ4IQJpykVFYoFqFZk7aH5NuoigZlanzDnMSozPkHamuwtubn0Ws9vT32m+aba189rXpUdWJYU7bluWw9bN7m4flKn1qHAX1ddmS9q7Zud9s+/G7oQbCLvz7b8Ru9tBOzhPZ798oFB/WQAMw4qz/1uXqW/cMz3KEjsp25T5fsMTMnfhvkOxGpLmfGxG2i9hQAAAXDSURBVJnMWzq3XbgCC4DL25eK0RuBcqfG/THhjUStla0sOJimHyptHqzkiMwG0DL3YYGrgjU17TtUhrem5qNBYF/x1tSW2RbiI5qWA8vLyhYGB7GaWzsqUzeR/OzkXdfc+4HNx+XuWHPP41/bcXog9viYepUXnPKc6SG5SGu5EWYW1lTP7Khcf03VZCXSt0etsXcxG9t07wtNz/zomWVr3whYuy1AYwmOH1TLiUP6sG2XYfsbev/xfbzHBtWEq5i/aBnzFy3jr0tfPqBwvNnxX0SEJnH0ntJJugNw4cSBSLiQPolHmXVWn07rOppI7f9FWB8dJz9m6tSoLd9IAy3CzcyaWO6es5fzJ54CGm2Fjx7Pf5iy3RRR+XFU9Src56+tna1repc5mBXW1Qz11tb+SoS7gJiI3h6sqblH17f18Vf4aWQrlC/666pv15V92kzVuqr2+MCvmYMyDnBV+ciUAY1L7p75yk3TK7Y4Iqi7rfnLQPl5a0UckUf8tdX/1iZYkXBd7a2KMxeI2u2al+jAyFj0fTbtaGHBil3syPpUV8kQ48g5WzZ6h37eu7LpIaLv0Z1BS83jzJhSbhC8YmKKGZOvI5AXgeHAOWj47AEfqqPNnDlFLJ/rIHY4Se9LZaFzl84nejs2gcrjzJh0Ge1niRlnvhdr5hNpY+Fp5i5acFDtdcOrmTXpYwf824+yqarkfFjzKCVX9iiyKItFeM1CWuBEOj438rg7MnPe3sNTqjjBupolwLs7SN8ELBBoUDgBOBuIRaRb65rMqTKCIoC/pvrriHyrgzIR8CxsQREjDNCOD+dkw1BP399jGTgoZ8XDYtaky1EePECKl4EXEAKUAcBkIvV9zWKdc5jfwfrlrXBWnDXpYZQyh1WgQBiO5dFn2o3plClgFxD9kgLYACyl9PI7lSjHzxIeomcyZ2lb7aQjZ8WD4U2P64gZRNDAynXQ4a5VGmG6wnUCH6Rj4QhE5Uf7nywUIbTYfwaaO8hTC1yyZ81zLlHCoeTUcPVe4QCIjW6+FSHKO3RPFuICQ0UY0qFwCKrwuTLhOJrMWfI7VH55gBSnANeifJjSeqUD24w8Tk3/w7P6HylC9yaIdM1J4jjlWknpJGCHqjUwDLgG+Bc6Fg5QvlImHEeQSINV6sSmV1XkXOBw7SJFUT7kjm76c/uIxKiWNSo6ndJscWgoOdDL4iMyZcLgjshMQMrXOAeLhfvjozL/fbj5D5v6wnWgDxx2fuEvFOIf6MqbPwCYv3ATItFu+jCDGZOvLAudu+SbqHY483eKcgvzlhz0aczDoUOLbnxk0wuhcCZEuhcfiOVqmOSOznT4o8dHNj9jsRNBnzroUoUVamRybHRz+12RUrRgYyMz7xLkbu3ow+FRKB6WLyRGZiKPqh51li3zqRp0NeiXgVyn6fehWZRbOGPJ+0ru9W8D3MJ3EaLPWoj+gIsnlx+Qmrf0G6h8oMN80byKmIuZt59t6ShxQPN+cmRmIzA9WN/jImvtNaJciFARkbQJ4TER80tneOMjchDXXiZGtaxR5Zxwbc1lKnwImEn5AiwAlghyt/NG030yLWIx3r5Do5qu0w09vhWE+mNUpwLRVldosFb+ErdNn5KTok7h7Y9EuZiD6XVkLmMrvf2/y/mTfoPVG0CuoLQIb08IPA/yEOLcxdyndtH5nSEgrInug0Se8dmHke1oRL7SgahyHl6WY9aUT4HeFBnv2QuB35SFz1v8ANOnP4LT+hGUSymtPduvTUJK50x+T5X+ggcX5Q/YdlgAku4kTaccku+VridR1JqBjpoBiE0S2pwbd7dE2j0OEd1EspivPs5xTX9Ek4rsiqUr1sqArYfwVo0od2WffmGsOM6qGYkQ2NCuS1RVPSeD3jgqZ5iPGOdP6Ie6/bHaC5wQ1V04NeuZM+ft6FJyZJk+PQ0tx2Gc0tFdG26HqteZP/+Q713rpptuuummm67h/wMOJfmluEry/gAAAABJRU5ErkJggg==")
        #set( $favico = "data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAArrAAAK6wGCiw1aAAAACXZwQWcAAAAgAAAAIACH+pydAAAJY0lEQVRYw63Ve5De1V3H8fe5/C7PfbOXbHazQJKShKhESWOTQgmD5ZKAgVBoKa0F0Y44Mk7/QG11qg4z1XHqOFNH2rGdjv7RDq04diplrAnYNBnoBCKXSCUhCQm72Vx299l99rn+bufiHxHHKpRm2u//53xe8z3nfI/gZ1ivnPgL6VlcXSmvecR5/1C7c/L43OJLv5+ZwbNrJ2/LAhV74wa8d8Pn3FtrxM8qfP/L9080Kpd/LM/5jcXF3qYsXRBBpYkO7VIlnHq+Xt7wchyOtJw33azozYyN6MNTI480f2rAk89tC+Ng9LpSfNljQeB+7szsNIcOlHnxhdfZdbfnfe+fIGQdQ+VfpBSvwuPopzMsd/9zX+5nP/1TAb77/J5hIQYPC8LPClRoXZs8t8ydm2Rhrs2VVyVMToyi5RRxMHax4bJDuzvN4X8/weqp8rmfCHDDnbfI9evWDIVRJf6nvUflwBwXT/ztdeOVins0LTq3WdvH48ErpFQEUUajPoRJyxR5jJQaS4Ipuhg3YHGxy5unB4yNVb+n3y18cvsmXSh5x/ml7n39tHX5iuGkfMs1V8l6dXS10kuNTq+PDgRKaqz1GOOwLiBPEqQ0ON/G2ZzCFCQDQ6tVkAw84+ONlycn4/vfFbB9y8bS8VPdh+eWzv+KDgbcvXsd9+2+BuMucLY5Q1wSzC9ozl+AqUmo1gxZajGFwLqUorAUmSNJLFnmkVJQq4fzlYr4g53b9519W8CW3TepchztmhodnfzhiVbnh6dne1dvjPnM7+zgnl3X8caZI7wxfZg40kRRxELTcfS4olE3rFjhKaTEGEOSeEzh8Qi0VkSRJIzk65WK+sN7btr/DPyYZ7jtQzu/labctdhactdsjuSffOpX2bppM4ePPsP0he9TimsoESOkoygEvb6lUlaMNdYhREyzc5JWawnvFMaCs55SLH8wtCL+3duv2/vSWzk/0oGtd9y84/KVYxNZLptPv/DafL2R8rE71svf++QdrBqb4OAr/8zC8iFK8RBSlLDO4oynVIJaLcObUWqVTcRRg8IlDJI2g54hS6Fa0+cuu6Ly4I6r/+X4/878EYAQfHh2ofPQhcV2svE95A/ffz2/tudWstRy4MXH6Q5eoxwPAyHeG4QQCOHp9z1CaKIwpZtMkxVDWNdnaCjC5GCto17Tj/7fcAC9ZfcHb18zMT7ujJree+jombiUBtdvGw0+9cCd7Ni6ndn5M5yY/leSYoY4GEKIAO8t3oP3HhAIPM5K8iJlsfUfIBTIAhDkBWgtZp1V3367o9Zay3vPzrc/cXauvTAxnrU+vufn3W/eu1OuXDHF0VMvMds8QGEWUDLGI8AbEAIQOAd4j/dgnMNkAiEyhPAINIPE0WnneMfX0sy03hbw0pGlF2TUu+f97x0e++2P7xy7bccNpLnjyPGnabafx/keUbiSUjiMFArnUjLTpfAG6aHIwTmP++8Z4DzgJEVhWG5nDHrWp4nd97lPv1K8LaBcW3rgwXs3q4c+uot1l13J2bkzzMw/S2fwKlIoRuq/TKN8JeAobJOiWAJh8b6LFR5jwRYWU4Cxnrxw5JkjTSy9ToFQ4rnGSHD0nV6b/utHP7T1rps+AGiOnX6ehfYrDNLTaF1hYvhGVlQ3kxfzdJNj5GYR5wcIadCBwmYGJcAjKKzFWcADwmOMJ4w1pYr6x02/FC28E0D08qf8fHOG2fmX6acz5EWLKBhiYvRm6qV1dJPjdAavkhZLmMKS55a88FjjCQJFuaLp9w2tpQxjHN5DYTymcEShem54NLr3vp37z75jB147+T2Wuiexrou1GaV4gokVOwh0jbnlf6M9eIMsTUlS6PcseWbIMtBKMj4RAx4hBXFJkaVg7cVxG8fqG9WS+tMfFw6gFztHcN7jnKMSTzJS34JxfZpLP6CfLtDrOPp9T557rPVYC0XuiOuSUkmx3CqYme5Rreo3JyZL+6zzx5KBe7FUCl+956anW7xLae8FpjBU4jHqlfcwyGbopKfo9xN6HShScB68hyy1pJmlXg+YmCgjhGDuQkqnUzDomxerZfVHn9izYVmIv7PvFvxWyTQ3RGGVOB6il5xiufc6neWUbkdgLXg8g76hOZ/S7RiG6iFr19Yo1zQL8wndTkYUK6QSKw8ePB9fSjiADmRIFFVIsjly0yMZOEwh8NbRWc7odg1Z5ogjxarVJaYmSwSRZO7cgAvnU5RWhKGkOT+4fGkpWwucvSRAFAcUpoexKUUB1ko67YzmQkZeOKQQ1BsBwyMxQw1NYRzNxYz5+RTvoVLRpKmh1compVSXXUo4gLamwAtDHKwAVzBzYY7mQo6SgjCUlEqaak0TBIIkcSy1cnptg7NQrmi0FDTnU9LUBSPD8epLBnQ6BWFkGGtMstxaYGF+QBRFSAlKCXQgcdaT9A3GQp47PFAqKZSW7vTJZXFmpidGRkvEsV5zqQA56FuSxJGaNgKB1p4wVASBQEqBs54ss6Spo8gdUkAYCioVNZsMiqeOv7582jtPqaQJIjn5mT/f3LgkgLGONIXl9hzj48MMDw9hTIZSEq0lF2frxQ9Qa4hCaaNYnyhXg0ce+eQLd1bK4ZOVaojDE4XqF8JQrb0kQJ47cIJuu0tum2zcsJ4w1Djn/gehL970Io7Umbgsvz6+qnLzR2/d/wTA+FT1O1GkloWHIBTr45K69pKPoLCevJA0F89RqRRctWE9Uai9ELYllT8RBDwXlvznGyN88MG7nv31O3fsnX5rg6kr6s+Wq8ETSikEgihSv/WFv9+++ScFiL/66rZUSoJKNcjCCFOrydroyCRJT5hWu/3tOFZf3HPjkweFEM57Hx44cvfKQbpctoUSzvve7g+sP/fZzx/aKIX/h3JVb67WNIGWB6NYPRaF8oRSoh2GKhldGbZ3XP3d5P8BvvT4tY876+es5VStqgdS+49EJW5s1BpBOV5xWqnogLUuUVLWPaacZM2RJO0veqeOIVgOQ3no9mufPvhnj215n1LyL6tVvSWMVFVrSRjKZhTL84GWs1KJprMc0lo8IwMxfdu2fRmA2Hv41j++ZesVfyPEV5YBvvy164dF7B8QIrs+joPxcqmqwyhKw0A0rM2vLIqil6bmKY9oRZE8pqR8c826+vc3T33LfvmbN9TSNPtwXFKrwlCaIJTj5bL+iFaMZLmfc9aviSL5zTBU+4XkO7u27zv/X74dvmmomOY7AAAAHnpUWHRDcmVhdGlvbiBUaW1lAAB42jPWNzTUNzQBAAViAVnfA1u2AAAAJXpUWHRTb2Z0d2FyZQAAeNpzTMlPSlVwyyxKLc8vyi5WcA42AwBEFQakROKmYgAAAABJRU5ErkJggg==")
        #set( $mainColor = "#800000")
        #set( $footer = "Powered by AnswerModules Beautiful WebForms")
        #set( $columns = 12)
        #if( $form.viewParams.title ) #set( $title = $form.viewParams.title) #end
        #if( $form.viewParams.subtitle1 ) #set( $subtitle1 = $form.viewParams.subtitle1) #end
        #if( $form.viewParams.subtitle2 ) #set( $subtitle2 = $form.viewParams.subtitle2) #end
        #if( $form.viewParams.format ) #set( $format = $form.viewParams.format) #end
        #if( $form.viewParams.date ) #set( $date = $form.viewParams.date) #end
        #if( $form.viewParams.version ) #set( $version = $form.viewParams.version) #end
        #if( $form.viewParams.logoID ) #set( $logo = "${url}?func=doc.Fetch&amp;nodeid=$form.viewParams.logoID") #end
        #if( $form.viewParams.logoURL ) #set( $logo = "$form.viewParams.logoURL") #end
        #if( $form.viewParams.mainColor ) #set( $mainColor = $form.viewParams.mainColor) #end
        #if( $form.viewParams.footer ) #set( $footer = $form.viewParams.footer) #end
        #if( $form.viewParams.containerwidth ) #set( $containerwidth = $form.viewParams.containerwidth) #end
        #if( $form.viewParams.previewDocID ) #set( $previewDoc = "$url/fetch/2000/doc.pdf?nodeid=$form.viewParams.previewDocID") #end
        #if( $form.viewParams.previewDocURL ) #set( $previewDoc = "$form.viewParams.previewDocURL") #end
        #if( $form.viewParams.amColumns ) #set( $columns = $form.viewParams.amColumns) #end
        
        
        <meta charset="utf-8">
        <title>$title</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
        <link rel="shortcut icon" href= $favico type="image/x-icon">
        <meta name="description" content="">
        <meta name="author" content="AnswerModules Sagl">
      
        #bwfCssResources([ 
        	['v2/css/am/am_form', "2.0.0"]
        	,['v2/css/font-awesome.min', "0.0.0"]
        	,['v2/css/metro-bootstrap-ECP.min', "0.0.0"]
        	,['v2/css/am/am_gridTable', "2.0.0"]
        	,["v2/css/select2/select2-bootstrap","3.5.4",
        		[
        			["v2/css/select2/select2","3.5.4"]
        		]
        	]
        ]) 
           #if( $form.viewParams.ajaxEnabled )
        	 #if(!$amgui.isAjaxRequest())
			 #bwfJsResources([ 
                       ['v2/js/am/am_ajaxvalidation.min', '2.0.0', [ 
                                                ['v2/js/am/am_init.min', '2.0.0', [ 
                                                                                ['v2/js/jquery.min','1.12.2'], 
        																		['v2/js/log4javascript', '1.4.13']
                                                                            ]
                                                ],
        										['v2/js/jquery.form', '3.51.0', [ 
                                                                                ['v2/js/jquery.min','1.12.2']
                                                                            ]
                                                ]
                                            ]	
                       ]
                     ,['v2/js/bootstrap.min','3.3.6',[['v2/js/jquery.min','1.12.2']]]
                     ,["v2/js/select2/select2_locale${params.lang}",'3.5.4',
                        [
                            ['v2/js/select2/select2.min','3.5.4',
        						[
        							['v2/js/jquery.min','1.12.2']
        						]
        					]
                        ]
                      ]
            ])
        	#end
        
            <script>
               
            </script>
        #else
        		#bwfJsResources([ 
        						   ['v2/js/am/am_validation.min', '2.0.0', [ 
        													['v2/js/am/am_init.min', '2.0.0', [ 
        																					['v2/js/jquery.min','1.12.2'], 
        																					['v2/js/log4javascript', '1.4.13']
        																				]
        													]
        												]	
        				  		   ]
        				 ,['v2/js/bootstrap.min','3.3.6',[['v2/js/jquery.min','1.12.2']]]
        				]) 
        	<script>
        		
        	</script>
        #end

        <!--end-->
        <style type="text/css">
            html, body {
                min-height:100%;  
            } 
            body {
                ##background-color: #e7f1f5;
                color: #333534;
            }
            
            label {
                font-weight: normal;
            }  

            .container {
                /*#if( ${containerwidth} )max-width: ${containerwidth}px; #end*/
                width: 1370px;
            }
            
            .inner-container{
                margin: 0 auto;
        	    
            }
            .content-row{
                margin: 20px 0;
            }
            
            .page-header {
                border-bottom: 1px solid $mainColor;
                margin: 20px 0;
                padding-bottom: 15px;
            }
            
            .am-form-title{
                float:right;
                color:$mainColor;
            }
            
            .nav-tabs li {
            	margin: 0 8px;
            }
            
            .nav-tabs li {
                border-color: #e6f7fe #e6f7fe #d8e2e6;
                background-color: #e6f7fe;
                color: #0078ca;
            }
            
            .nav-tabs li.active {
                
                background-color: #fff;
                
            }
            
            .panel-heading {
                border-radius:0;
            	
            }
            
            .panel-heading a:after {
                font-family: 'Glyphicons Halflings';
                content: "\e114";    
                float: right; 
                color: grey; 
            }
            .panel-heading a.collapsed:after {
                content: "\e080";
            }
            
            
             .select2-container .select2-choice,  .select2-container-multi .select2-choices {
                background-image: none;
                border: 1px solid #cfcfcf;
                border-radius: 0;
                /*height: 22px;*/
            }
            

            .amGridContainer .select2-container .select2-choice, .amGridContainer .select2-container-multi .select2-choices {
                padding: 0 0 0 0px;
            }
            
            .has-error .select2-choice {
                border-color: #B94A48;
            }
            
            .select, select.form-control {
                background: none repeat scroll 0 0 #ffffff;
                border: 1px solid #cfcfcf;
                border-radius: 0px;
                box-shadow: none;
                color: #0a2937;
                font-size: 14px;
            }
            
            /* Checkbox align */
            
            .checkbox .checkbox-label {
                display: inline-block;
            }
            
            /* Style Add/Delete Row component */
            
            .panel{
                position:relative;
            }
            
            .panel .am-form-adddelete-row.am-panel-heading {
                position: absolute;
                right: 9px;
                top: 9px;
            }
             .radio-inline + .radio-inline, .checkbox-inline + .checkbox-inline {
                margin-left:0px;
            }
            
            .checkbox input[type="checkbox"]:checked ~ .checkbox-label::after {
                top:11px;
                left: 1px;
            }
            
            
            /* Customizations */
            
            body {
                font-size:16px;
            }
            
            .panel-heading {
                padding-top: 3px;
                padding-bottom: 3px;
            }
            .form-control-static {
                border-bottom: 1px dashed #ccc;
                height:34px;
            }
            .am-form-tsubt-subtitle {
              font-style:italic;
            }
            
            /* Required for the change in body font size */
            .radio-inline input[type=radio]:checked~.radio-label:after, .radio input[type=radio]:checked~.radio-label:after {
    			margin-top: -6px;
			}
            
            .am-form-required:after {
                content: "\002a";
                color: #999;
                font-size: 0.6em;
                vertical-align: top;
                padding-top: 4px;
                padding-right:0px;
            }
            
            .help-block {
                padding-top: 5px;
                font-size: 0.8em;
            }
            
            /* Fixes */
            .am-validation-error-list a{
                color:#fff;
            }
            
            .am-validation-error-list p:last-child {
                margin: 0;
            }
            
            .am-form-box-container > .form-group:last-child {
                margin-bottom: 0;
            }
            
            table{
                text-align: center;
                width: 100%;
            }
            
            table, tr, td{
                border: 1px solid;
                border-color: #BAD405;
            }
            
        </style>
        
        
    </head>
    
    <body>
        

        
        <div class="container" style="/*border-bottom:2px solid #576A2D*/">
            <div class="inner-container">
                <div class="row content-row">
                    <div class="col-xs-12">
                        <!--
                        <div class="col-sm-3" style="padding-right:30px;">
                        	<img src="$!logo" style="max-height:100px;" class="am-form-logo img-responsive" />
                        </div>
                        <h1 class="col-sm-9 text-center"><span style="font-size: 32px;">$!title</span></h1>
						!-->
                        <table>
							<tr>
								<td rowspan="3" colspan="2" align="center"><img src="$!logo" style="max-height:100px;" class="am-form-logo img-responsive" /></td>
                                <td colspan="3" ><h1><span style="font-size: 28px;">$!title</span></h1></td>
							</tr>
							<tr>
								<td colspan="3"><h4><span style="font-size: 20px;">$!subtitle1</span><BR>$!subtitle2</h4></td>
							</tr>
							<tr>
                                <td>$!format</td>
								<td>Elaborado<BR>$!date</td>
                                <td>Versión: $!version</td>
							</tr>
						</table>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="container">
            <div class="inner-container">
                
                <div class="row content-row">
					 #if( $form.viewParams.ajaxEnabled )
                    <div id="loadingindicator" style="background: none repeat scroll 0px 0px rgba(255, 255, 255, 0.85);
                                                      z-index: 1000; bottom: 1px; left: 1px; position: fixed; right: 0px; top: 0px; 
                                                      margin-left: 0px; margin-top: 0px;" class="hidden">
                        <div style="height: 60px;left: 0;margin-top: -30px;position: fixed;right: 0;text-align: center;top: 50%;">
                            <i class="fa fa-circle-o-notch fa-spin fa-3x"></i>
                            <br>
                            <p>CARGANDO...</p>
                        </div>
                    </div>
                    #end                      
                    <div class="col-md-12" id="mainForm">
                   

                        <am:form />
                    </div>				
                </div>
            </div>
            
            <hr class="hidden-print"/>
            
            <div class="footer hidden-print">
                <small>$footer</small>
            </div>
            
        </div>
        
    </body>
</html>
