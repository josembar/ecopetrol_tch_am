
//EIM Developer: José Barrantes

log.error("Inicio actualización binder principal WF Documental")

//variable para capturar la carpeta de adjuntos del workflow
def adjuntosFolder

//variable para obtener los atributos del workflow
def atributos

//variable para capturar el estado del workflow
def workflowStatus

//obtener información del workflow
try
{
    
    //obtener el estado del workflow
    workflowStatus = workflow.getWorkflowStatus(workID,subWorkID)
    
    //obtener los atributos del workflow
    atributos = workflowStatus.getAttributes()
    
    //obtener la carpeta de adjuntos del workflow
    adjuntosFolder = workflowStatus.getAttachmentsFolder()
    
}
catch (e)
{
    log.error("Error consultando informacion del WF Documental: ${workID}", e)
}

//variable para obtener el dataid del documento que va dentro del workflow
def docID

//variable para obtener el No Expediente del documento que va dentro del workflow
def idCaso

//variable para obtener el dataid del binder principal
def binderID

//obtener el dataid del binder principal
try
{
    //se guarda el dataid del documento adjunto en el workflow
    docID = docman.getChildrenFast(adjuntosFolder).get(0).ID
    //docID = docman.getChildrenFast(asCSNode(2139472 as long)).get(0).ID
    
    log.error("doc: ${docID}")
    
    //si el adjunto es un documento
    if (asCSNode(docID as long).subtype == 144)
    {
        log.error("El adjunto es un documento...")
        
        idCaso = asCSNode(docID as long)."Solicitud Servicio EC"."No Expediente".toString()
    
    	log.error("idCaso: ${idCaso}")
    
    	//query para obtener el dataid del binder principal
    	def queryBinderPrincipal = """select binderid
								  	  from z_li_gab40
								  	  where idcaso = '${idCaso}'
							   	   """
    	//se ejecuta el query queryBinderPrincipal y el resultado se guarda en la variable binderID
    	binderID = sql.runSQLFast(queryBinderPrincipal,false,false,0).rows.collect{it.BINDERID}.get(0)
    	log.error("binderID: ${binderID}")
    }
    
    //si el adjunto es un shortcut
    if (asCSNode(docID as long).subtype == 1)
    {
        log.error("El adjunto es un shortcut...")
        
        //query para obtener el dataid del documento original
        def queryOrigen = """select origindataid origen
							 from dtree 
							 where dataid = ${docID} 
  							 and deleted = 0
						  """
        //variable para obtener el dataid del documento original
        def docOrigen = sql.runSQLFast(queryOrigen,false,false,0).rows.collect{it.ORIGEN}.get(0)
        
        binderID = asCSNode(-asCSNode(docOrigen as long).parent.parent.parent.ID as long).ID
        log.error("binderID: ${binderID}")
    }
    
}

catch (e)
{
    //log.error("Error obteniendo el dataid del binder principal. WF Documental: ${}", e)
    log.error("Error obteniendo el dataid del binder principal. WF Documental: ${workID}", e)
}

//variable para capturar el CSNode del binder de la solicitud creada a través del gab40
def binderPrincipal

//actualizar la información del binder principal
try
{
    
    binderPrincipal = asCSNode(binderID as long)
    log.error("binderPrincipal: ${binderPrincipal}")
    
    binderPrincipal."Solicitud Servicio EC"."Estado" = atributos.data.EstadoProceso
    //binderPrincipal."Solicitud Servicio EC"."Estado" = "Cupo Venta Aprobado"
    
    //se actualizan los metadatos del binder
	binderPrincipal.update()
    
    log.error("Nuevo estado binder principal: ${binderPrincipal."Solicitud Servicio EC"."Estado".toString()}")
}

catch (e)
{
	//log.error("Error actualizando informacion del binder principal. WF Documental: ${}", e) 
	log.error("Error actualizando informacion del binder principal. WF Documental: ${workID}", e) 
}

log.error("Fin actualización binder principal WF Documental")


