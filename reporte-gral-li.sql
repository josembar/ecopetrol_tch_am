select gab40.nombresolicitante,
gab40.correoelectronico,
gab40.idcaso idcasogab40,
gab40.lugarrecoleccion,
gab40.tiposolicitud tiposolicitudgab40,
gab51.subasta,
gab51.fechasubasta,
gab51.oferente,
gab51.oferenteid,
gab51detalle.lote lotegab51d,
gab51detalle.descripcion,
gab51detalle.cantaprox,
gab51detalle.unidad,
gab51detalle.vupma valorunitario,
gab51detalle.vtplpma valortotal,
gab54.comprador,
gab54.identificacion,
gab54.contacto,
gab54.noorden,
gab54.lugarretirodetalle,
gab54.funcionarioaseg,
gab54.funcionarioasegcontacto,
gab54.plazoretirodesde,
gab54.plazoretirohasta,
gab54.solicitadopor,
gab54.gestionadopor,
gab54.autorizadopor,
gab54.tiposolicitud tiposolicitudgab54,
gab54.nosolicitudretiro,
gab54.sociedad,
gab54items.lote lotegab54i,
gab54items.unidad unidadgab54i,
gab54items.descripcion descgab54,
gab54items.cantidad cantgab54
from z_li_gab40 gab40 
inner join z_li_gab51 gab51
on gab51.idcaso = gab40.idcaso
inner join z_li_gab51detalle gab51detalle
on gab51detalle.volumeid = gab51.volumeid
inner join z_li_gab54 gab54
on gab54.idcaso = gab51.idcaso
inner join z_li_gab54items gab54items
on gab54items.volumeid = gab54.volumeid
where gab40.idcaso = 'FNX000190'

