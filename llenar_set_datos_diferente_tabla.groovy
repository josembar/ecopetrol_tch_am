

//EIM Developer: José Barrantes

//variable para almacenar el código FNX de la solicitud
def idCaso = form.idcaso.value
log.error("GAB54, el N° del caso es: ${idCaso}")

//query para traer el detalle de la requisición de pago
def queryDetalle = """select detallegab51.lote,
					  detallegab51.descripcion,
					  detallegab51.cantaprox,
					  detallegab51.unidad,
					  detallegab51.vupma,
					  detallegab51.vtplpma
					  from z_li_gab51detalle detallegab51
					  inner join z_li_gab51 gab51
					  on detallegab51.volumeid = gab51.volumeid
					  where gab51.idcaso = 'FNX000151'
				   """

//variable para almacenar el resultado del query queryDetalle
def detalle = sql.runSQLFast(queryDetalle,false,false,0).rows
log.error("El detalle es: ${detalle}")
log.error("El tamaño de detalle es: ${detalle.size()}")
//log.error("El tamaño del set es: ${form.detalle.size()}")

try
{
    //ciclo para ir llenando rows dentro del set con cada resultado contenido en la variable detalle

for (int i = 0; i < detalle.size(); i++)
{
    //log.error("Lote: ${detalle.collect{it.LOTE}.get(i)}")
    
    //log.error("Descripcion: ${detalle.collect{it.DESCRIPCION}.get(i)}")
    
    //log.error("Cant. Aprox: ${detalle.collect{it.CANTAPROX}.get(i)}")
    
    //log.error("Unidad: ${detalle.collect{it.UNIDAD}.get(i)}")
    
    //log.error("Valor unitario: ${detalle.collect{it.VUPMA}.get(i)}")
    
    //log.error("Valor total: ${detalle.collect{it.VTPLPMA}.get(i)}")  

    form.addField("detalle",form.detalle.size())
    form.detalle.lote[i].value = detalle.collect{it.LOTE}.get(i)
    form.detalle.descripcion[i].value = detalle.collect{it.DESCRIPCION}.get(i)
    form.detalle.cantaprox[i].value = detalle.collect{it.CANTAPROX}.get(i)
    form.detalle.unidad[i].value = detalle.collect{it.UNIDAD}.get(i)
    form.detalle.vupma[i].value = detalle.collect{it.VUPMA}.get(i)
    form.detalle.vtplpma[i].value = detalle.collect{it.VTPLPMA}.get(i)
}

}
catch (e)
{
    log.error("error llenando set : ",e)
}


/*
Como el tamaño del set siempre arranca en 1, luego de agregar todos los resultados queda una última línea vacía.
La siguiente línea de código elimina esa línea
*/
//form.removeField("detalle",form.detalle.size() - 1)

