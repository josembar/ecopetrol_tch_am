/*
████████╗███████╗ ██████╗██╗  ██╗███████╗██████╗  ██████╗ ███████╗    ████████╗███████╗ █████╗ ███╗   ███╗
╚══██╔══╝██╔════╝██╔════╝██║  ██║██╔════╝██╔══██╗██╔════╝ ██╔════╝    ╚══██╔══╝██╔════╝██╔══██╗████╗ ████║
   ██║   █████╗  ██║     ███████║█████╗  ██║  ██║██║  ███╗█████╗         ██║   █████╗  ███████║██╔████╔██║
   ██║   ██╔══╝  ██║     ██╔══██║██╔══╝  ██║  ██║██║   ██║██╔══╝         ██║   ██╔══╝  ██╔══██║██║╚██╔╝██║
   ██║   ███████╗╚██████╗██║  ██║███████╗██████╔╝╚██████╔╝███████╗       ██║   ███████╗██║  ██║██║ ╚═╝ ██║
   ╚═╝   ╚══════╝ ╚═════╝╚═╝  ╚═╝╚══════╝╚═════╝  ╚═════╝ ╚══════╝       ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝     ╚═╝
                                            ╦╔╗╔╔═╗╔═╗╦╦═╗╦╔╗╔╔═╗  ╔╦╗╦═╗╦ ╦╔═╗╔╦╗  ╔═╗╦  ╔═╗╔╗ ╔═╗╦  ╦ ╦ ╦
                                            ║║║║╚═╗╠═╝║╠╦╝║║║║║ ╦   ║ ╠╦╝║ ║╚═╗ ║   ║ ╦║  ║ ║╠╩╗╠═╣║  ║ ╚╦╝
                                            ╩╝╚╝╚═╝╩  ╩╩╚═╩╝╚╝╚═╝   ╩ ╩╚═╚═╝╚═╝ ╩   ╚═╝╩═╝╚═╝╚═╝╩ ╩╩═╝╩═╝╩ 
*/
/***** Editar Permisos Rol Acuerdo de Cooperación BEGIN - DGC 27/02/18*****/
	Date inicio = new Date();
 	app = docman.getScriptByNickname('am_convenios_config').getCSVars()?.app
    //DEFINIR LOS PERMISOS DEL ROL EN LOS BINDER
    def Rigths = ['SEE','SEECONTENTS','MODIFY','EDITATTRIBUTES','ADDITEMS']
    //SELECCCIONAR TODOS LOS BINDER COMENZANDO EN LA CARPETA 01.CONVENIOS COMO PADRE
    String querydtreecore = """SELECT dt.dataid
                               FROM dtreecore dt
                               WHERE deleted=0 AND subtype='31066' 
                               start with dt.dataid = ${app.constants.parentConvenios}
                               connect by abs(dt.parentid) = prior dt.dataid""" 
	def dtdataid = sql.runSQLFast(querydtreecore,false,false,1).rows
	out<<"Numero total de Binders encontrados: ${dtdataid.size()}<br>"
	//SELECCIONAR TODOS LOS RIGHTID DE CADA BINDER
	def rightidrol = [[:]]
    for(int i=0;i<dtdataid.size();i++){
        def rightidnode = docman.getRights(asCSNode(dtdataid.get(i).get('DATAID'))).ACLRights.toArray()
        for(int j=0;j<rightidnode.size();j++){
             String[] temp = String.valueOf(rightidnode[j]).split('=')
			 String[] temp2 = temp[1].split(',')
             //SELECCIONAR LOS RIGHTID DE CADA BINDER QUE CONTENGAN EL NOMBRE DEL ROL ACUERDO DE COOPERACIÓN
             if(users.getMemberById(temp2[0] as long).displayName.contains("Rol Acuerdo Cooperación")){
                 rightidrol<<['ID':dtdataid.get(i).get('DATAID'),'rightid':temp2[0]]
    		 }
        }
    }
	out<<"Numero total de RightID encontrados: ${rightidrol.size()-1}<br>"
	//ITERAR CON CADA ID Y RIGHTID
	for(int k=1;k<rightidrol.size();k++){
        try{
            long IDOBJ = rightidrol.get(k).get('ID') as long
            long RIGHTID = rightidrol.get(k).get('rightid') as long
            //CAMBIAR PERMISO PARA EL NODO ID Y ROL RIGHTID
            docman.grantRights(docman.getNode(IDOBJ), users.getMembersByID(RIGHTID), Rigths)
            out<<"Permisos Editados - ID:${IDOBJ} - RIGHTID:${RIGHTID}<br>"
            try{
                //CAMBIAR PERMISO PARA EL NODO -ID Y ROL RIGHTID
                docman.grantRights(docman.getNode(-IDOBJ), users.getMembersByID(RIGHTID), Rigths)
                 out<<"Permisos Editados - ID:${-IDOBJ} - RIGHTID:${RIGHTID}<br>"
            }
            catch(f){
                out<<"Error - ID:${-IDOBJ} - RIGHTID:${RIGHTID}<br>"
            }
        }
        catch(e){
            out<<"Error - ID:${IDOBJ} - RIGHTID:${RIGHTID}<br>"
        }
	}
	out<<"Hora Inicio = ${inicio.format('h:mm:ss a')} - Hora Fin = ${new Date().format('h:mm:ss a')}"
/***** Editar Permisos Rol Acuerdo de Cooperación END - DGC 27/02/18*****/