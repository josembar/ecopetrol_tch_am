/*
████████╗███████╗ ██████╗██╗  ██╗███████╗██████╗  ██████╗ ███████╗    ████████╗███████╗ █████╗ ███╗   ███╗
╚══██╔══╝██╔════╝██╔════╝██║  ██║██╔════╝██╔══██╗██╔════╝ ██╔════╝    ╚══██╔══╝██╔════╝██╔══██╗████╗ ████║
   ██║   █████╗  ██║     ███████║█████╗  ██║  ██║██║  ███╗█████╗         ██║   █████╗  ███████║██╔████╔██║
   ██║   ██╔══╝  ██║     ██╔══██║██╔══╝  ██║  ██║██║   ██║██╔══╝         ██║   ██╔══╝  ██╔══██║██║╚██╔╝██║
   ██║   ███████╗╚██████╗██║  ██║███████╗██████╔╝╚██████╔╝███████╗       ██║   ███████╗██║  ██║██║ ╚═╝ ██║
   ╚═╝   ╚══════╝ ╚═════╝╚═╝  ╚═╝╚══════╝╚═════╝  ╚═════╝ ╚══════╝       ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝     ╚═╝
                                            ╦╔╗╔╔═╗╔═╗╦╦═╗╦╔╗╔╔═╗  ╔╦╗╦═╗╦ ╦╔═╗╔╦╗  ╔═╗╦  ╔═╗╔╗ ╔═╗╦  ╦ ╦ ╦
                                            ║║║║╚═╗╠═╝║╠╦╝║║║║║ ╦   ║ ╠╦╝║ ║╚═╗ ║   ║ ╦║  ║ ║╠╩╗╠═╣║  ║ ╚╦╝
                                            ╩╝╚╝╚═╝╩  ╩╩╚═╩╝╚╝╚═╝   ╩ ╩╚═╚═╝╚═╝ ╩   ╚═╝╩═╝╚═╝╚═╝╩ ╩╩═╝╩═╝╩ 
*/
/***** Reporte Excel GAB34- José Barrantes 29/03/2019 BEGIN *****/


import java.text.SimpleDateFormat
import java.util.Date
import java.math.BigDecimal

//Nombre pestanas
VALORES = "VALORES_UNICOS"
REVISOR = "REVISOR"
APROBADOR = "APROBADOR"
DETMINV = "DETALLE MAT. INV."
ACUM = "ACUMULADOS"
SINCONINI = "SOBRANTES INICIALES"
FINCONINI = "FALTANTES INICIALES"
SINCONFIN = "SOBRANTES FINALES"
FINCONFIN = "FALTANTES FINALES"
ACCREQ = "ACCIONES REQUERIDAS"
HALLUBI = "HALLAZGOS UBICACION"
HALLPRE = "HALLAZGOS PRESERVACION"
PART = "PARTICIPANTES INV." 

try{
    //parametros del reporte
    String fechainicial = ""
    params.fechainicial == null ? null : (fechainicial = params.fechainicial.toString())
    
    String fechafinal = ""
    params.fechafinal == null ? null : (fechafinal = params.fechafinal.toString())
    
    String cond1 = ""
    String cond2 = ""
    
    if(fechainicial != ""){
        cond1 = "AND FECHADOCUMENTO >=  TO_DATE('${fechainicial}','DD/MM/YYYY')"
    }
    
    if(fechafinal != ""){
        cond2 = "AND FECHADOCUMENTO <=  TO_DATE('${fechafinal}','DD/MM/YYYY')"
    }
    
    String query = """SELECT SEQ, WORKID
                      FROM Z_ADB_GAB34
                      WHERE ROWSEQNUM = 1
					  ${cond1}
					  ${cond2}
                   """
    def WF = sql.runSQLFast(query, false, false, 0).rows
    
    //crea un libro con sus pestanas
    String[] secciones = [VALORES, REVISOR, APROBADOR, DETMINV, ACUM, SINCONINI, FINCONINI, SINCONFIN, FINCONFIN, ACCREQ,
                          HALLUBI, HALLPRE, PART]
    def archivoexcel = xlsx.createSpreadsheet(secciones)
    //Escribe los titulos de cada pestaña en el excel
    List<String> TVALORES = ["Fecha de Documento","Vicepresidencia","Año de Vigencia","Solicitante","Rol Solicitante", 
                             "Objetivos", "Valor Total", "Cantidad Total","PorCum","Otros","Otros2","Otros3","Otros4",
                             "HallRecom","Adicional","Adicionales"] 
    archivoexcel.getWorksheetByName(VALORES).writeRow(TVALORES,0)
    
    List<String> TREVISOR = ["Fecha de Documento","Usuario", "Rol"] 
    archivoexcel.getWorksheetByName(REVISOR).writeRow(TREVISOR,0) 
    
    List<String> TAPROBADOR = ["Fecha de Documento","Usuario", "Rol"] 
    archivoexcel.getWorksheetByName(APROBADOR).writeRow(TAPROBADOR,0)
    
    List<String> TDETMINV = ["Fecha de Documento","Centro Logístico","Almacén","Localización","Cantidad de Cód. Inv.",
                             "Valor Total"] 
    archivoexcel.getWorksheetByName(DETMINV).writeRow(TDETMINV,0) 
    
    List<String> TACUM = ["Fecha de Documento","% del inventario total cubierto en cantidad","% del inventario total cubierto en valor"] 
    archivoexcel.getWorksheetByName(ACUM).writeRow(TACUM,0)
    
    List<String> TSINCONINI = ["Fecha de Documento","Centro Logístico","Almacén","Material","Tipo Material","Documento Inventario",
                               "Unidad de Medida","Valor Unitario","Cantidad Sistema","Cantidad Física","Diferencias en Cantidad",
                               "Valor Diferencias"] 
    archivoexcel.getWorksheetByName(SINCONINI).writeRow(TSINCONINI,0)
    
    List<String> TFINCONINI = ["Fecha de Documento","Centro Logístico","Almacén","Material","Tipo Material","Documento Inventario",
                               "Unidad de Medida","Valor Unitario","Cantidad Sistema","Cantidad Física","Diferencias en Cantidad",
                               "Valor Diferencias"] 
    archivoexcel.getWorksheetByName(FINCONINI).writeRow(TFINCONINI,0)
    
    List<String> TSINCONFIN = ["Fecha de Documento","Centro Logístico","Almacén","Material","Tipo Material","Documento Inventario",
                               "Valor Unitario","Cantidad Sistema","Cantidad Física","Diferencias en Cantidad","Valor Diferencias"] 
    archivoexcel.getWorksheetByName(SINCONFIN).writeRow(TSINCONFIN,0)
    
    List<String> TFINCONFIN = ["Fecha de Documento","Centro Logístico","Almacén","Material","Tipo Material","Documento Inventario",
                               "Valor Unitario","Cantidad Sistema","Cantidad Física","Diferencias en Cantidad","Valor Diferencias"] 
    archivoexcel.getWorksheetByName(FINCONFIN).writeRow(TFINCONFIN,0)
    
    List<String> TACCREQ = ["Fecha de Documento","Acción","Responsable","Fecha Proyectada","Observaciones del Avance","Estado"] 
    archivoexcel.getWorksheetByName(ACCREQ).writeRow(TACCREQ,0)
    
    List<String> THALLUBI = ["Fecha de Documento","Código","Nombre Material","Hallazgos"] 
    archivoexcel.getWorksheetByName(HALLUBI).writeRow(THALLUBI,0)
    
    List<String> THALLPRE = ["Fecha de Documento","Código","Nombre Material","Hallazgos"] 
    archivoexcel.getWorksheetByName(HALLPRE).writeRow(THALLPRE,0)
    
    List<String> TPART = ["Fecha de Documento","Nombre Completo","Rol"] 
    archivoexcel.getWorksheetByName(PART).writeRow(TPART,0)
    
    //Declaramos los contadores
    int count = 2
    int count2 = 2
    int count3 = 2
    int count4 = 2
    int count5 = 2
    int count6 = 2
    int count7 = 2
    int count8 = 2
    int count9 = 2
    int count10 = 2
    int count11 = 2
    int count12 = 2
    int count13 = 2
    
    //Patrones para formatear capos fecha
    SimpleDateFormat sdf=new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy")
    SimpleDateFormat sdf2=new SimpleDateFormat("dd/MM/yyyy")
    
    //String para quitar tags html de campos multiline
    String strRegEx = "<[^>]*>"
    
    //Inicio iteración para llenar los datos en las pestañas del excel
    for(int i = 0; i<WF.size(); i++){
    //Escribe datos Pestaña VALORES UNICOS
        List<String> Encabezado = new ArrayList<String>()
        
        boolean pasa = true
        
        try {
          form = workflow.getWorkflowStatus(WF.get(i).get("WORKID") as long).getFormData("Form")  
            
        	}
        catch(e){
           pasa = false 
       		}
        if (pasa) {
        
                Encabezado.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
            	//vicepresidencia
            	def query10 = """SELECT NOMBRE
							     FROM Z_CVN_DEPENDENCIAS1	
                                 WHERE ACTIVO = 1
								 AND SIGLA = '${form.Vicepresidencia.value}'
                              """
            	query101=sql.runSQLFast(query10, false, false, 1).rows
            	Encabezado.add(query101.get(0).get("NOMBRE"))
            
                Encabezado.add(form.AnoVigencia.value.toString())
            
                Encabezado.add(users.getUserById(form.Solicitante.value as long)?.displayName)
                     
                def query11 = """SELECT DESCRIPCIONLARGA
                                    FROM Z_ADB_Parametros
                                    WHERE Tipo = 'Rol' 
                                    AND Sigla ='${form.RolSolicitante.value.toString()}'
                                """      
                query111=sql.runSQLFast(query11, false, false, 1).rows
                Encabezado.add(query111.get(0).get("DESCRIPCIONLARGA"))                
                
                Encabezado.add(form.Objetivos.value.toString().replaceAll(strRegEx, ""))
            
            	//cantidad total
            	Encabezado.add(form.CantidadTotal.value.toString())
            
            	//valor total
            	Encabezado.add(form.ValorTotal.value.toString())
            
            	//PorcCum
            	Encabezado.add(form.PorcCum.value.toString())
            
            	//Otros
            	Encabezado.add(form.Otros.value.toString().replaceAll(strRegEx, ""))
            
            	//Otros2
            	Encabezado.add(form.Otros2.value.toString().replaceAll(strRegEx, ""))
            
            	//Otros3
            	Encabezado.add(form.Otros3.value.toString().replaceAll(strRegEx, ""))
            
            	//Otros4
            	Encabezado.add(form.Otros4.value.toString().replaceAll(strRegEx, ""))
            	
            	//HallRecom
            	Encabezado.add(form.HallRecom.value.toString().replaceAll(strRegEx, ""))
                
                Encabezado.add((form.Adicional.value.toString())== "true" ? "SI":"NO")
                
            	Encabezado.add(form.Adicionales.value.toString().replaceAll(strRegEx, ""))
                
        
                archivoexcel.getWorksheetByName(VALORES).writeRow(Encabezado,i+2)
            
        //Escribe datos Pestaña REVISOR
        for(int j = 0; j<form.Revisor.size(); j++)
		{
            List<String> Revisores = new ArrayList<String>()
            Revisores.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            Revisores.add(users.getUserById(form.Revisor[j].Usuario.value as long)?.displayName)
            
            def query13 = """SELECT DESCRIPCIONLARGA
                            FROM Z_ADB_Parametros
                            WHERE Tipo = 'Rol' 
                            AND Sigla ='${form.Revisor[j].Rol.value.toString()}'
                        """      
            query131=sql.runSQLFast(query13, false, false, 1).rows
            Revisores.add(query131.get(0).get("DESCRIPCIONLARGA"))
            
            archivoexcel.getWorksheetByName(REVISOR).writeRow(Revisores,count++)
        }
            
        //Escribe datos Pestaña APROBADOR
        for(int k = 0; k<form.Aprobador.size(); k++)
		{
            List<String> Aprobadores = new ArrayList<String>()
            Aprobadores.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            Aprobadores.add(users.getUserById(form.Aprobador[k].Usuario.value as long)?.displayName)
            
            def query14 = """SELECT DESCRIPCIONLARGA
                            FROM Z_ADB_Parametros
                            WHERE Tipo = 'Rol' 
                            AND Sigla ='${form.Aprobador[k].Rol.value.toString()}'
                        """      
            query141=sql.runSQLFast(query14, false, false, 1).rows
            Aprobadores.add(query141.get(0).get("DESCRIPCIONLARGA"))
                        
            archivoexcel.getWorksheetByName(APROBADOR).writeRow(Aprobadores,count2++)
        }
         
        //Escribe datos Pestaña DETMINV
        for(int l = 0; l<form.DetMInv.size(); l++)
		{
           List<String> DetMInv = new ArrayList<String>() 
            
           //fecha del documento
           DetMInv.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
           
           //centro logistico
           def query15 = """SELECT CONCAT(CONCAT(Centro,'-'),NombreCentro) AS CENTROLOGISTICO
						 	 FROM Z_EC_CENTROLOG
						     WHERE Centro = '${form.DetMInv[l].CentroLogistico.value.toString()}'
					      """
           query151=sql.runSQLFast(query15, false, false, 1).rows
           DetMInv.add(query151.get(0).get("CENTROLOGISTICO"))
            
           //almacen
           def query16 = """SELECT NOMBREALMACEN
                             FROM Z_EC_ALMACEN
                             WHERE SIGLA  = '${form.DetMInv[l].Almacen.value.toString()}'
                         """      
           query161=sql.runSQLFast(query16, false, false, 1).rows
           DetMInv.add(query161.get(0).get("NOMBREALMACEN"))   
            
           //localización
           DetMInv.add(form.DetMInv[l].Localizacion.value.toString())
            
           //Cantidad de Cód. Inv.
           DetMInv.add(form.DetMInv[l].CodInventariados.value.toString())
            
           //valor total
           DetMInv.add(form.DetMInv[l].Valor.value.toString())
            
           archivoexcel.getWorksheetByName(DETMINV).writeRow(DetMInv,count3++)
        }
            
        //Escribe datos Pestaña ACUM
        for(int m = 0; m < form.Acum.size();m++)
		{
			List<String> Acum  = new ArrayList<String>() 
            
            //fecha del documento
            Acum.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
            //item
            Acum.add(form.Acum[m].Item.value.toString())
            
            //% del inventario total cubierto en cantidad
            Acum.add(form.Acum[m].PorInvCc.value.toString())
            
            //% del inventario total cubierto en valor
            Acum.add(form.Acum[m].PorInvCv.value.toString())
            
            archivoexcel.getWorksheetByName(ACUM).writeRow(Acum,count4++)
        }
            
		//Escribe datos Pestaña SINCONINI
		for(int n = 0; n < form.SInconIni.size();n++)
		{
           List<String> SInconIni = new ArrayList<String>() 
            
           //fecha del documento
           SInconIni.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
           //Centro Logístico
           def query17 = """SELECT CONCAT(CONCAT(Centro,'-'),NombreCentro) AS CENTROLOGISTICO
						 	 FROM Z_EC_CENTROLOG
						     WHERE Centro = '${form.SInconIni[n].CentroLogistico.value.toString()}'
					      """
           query171=sql.runSQLFast(query17, false, false, 1).rows
           SInconIni.add(query171.get(0).get("CENTROLOGISTICO"))
            
           //Almacén
           def query18 = """SELECT NOMBREALMACEN
                             FROM Z_EC_ALMACEN
                             WHERE SIGLA  = '${form.SInconIni[n].Almacen.value.toString()}'
                         """      
           query181=sql.runSQLFast(query18, false, false, 1).rows
           SInconIni.add(query181.get(0).get("NOMBREALMACEN"))   
            
           //Material
           SInconIni.add(form.SInconIni[n].Material.value.toString())
            
           //Tipo Material
           SInconIni.add(form.SInconIni[n].TipoMaterial.value.toString())
            
           //Documento Inventario
           SInconIni.add(form.SInconIni[n].Documento.value.toString())
            
           //Unidad de Medida
           SInconIni.add(form.SInconIni[n].UnidadMed.value.toString())
            
           //Valor Unitario
           SInconIni.add(form.SInconIni[n].ValorUni.value.toString())
            
           //Cantidad Sistema
           SInconIni.add(form.SInconIni[n].CantidadSis.value.toString())
            
           //Cantidad Física
           SInconIni.add(form.SInconIni[n].CantidadFi.value.toString())
            
           //Diferencias en Cantidad
           SInconIni.add(form.SInconIni[n].DiferenCan.value.toString())
            
           //Valor Diferencias
           SInconIni.add(form.SInconIni[n].DiferenVal.value.toString())
            
           archivoexcel.getWorksheetByName(SINCONINI).writeRow(SInconIni,count5++)
        }
            
		//Escribe datos Pestaña FINCONINI
		for(int o = 0; o < form.FInconIni.size();o++)
		{
           List<String> FInconIni = new ArrayList<String>() 
            
           //fecha del documento
           FInconIni.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
           //Centro Logístico
           def query19 = """SELECT CONCAT(CONCAT(Centro,'-'),NombreCentro) AS CENTROLOGISTICO
						 	 FROM Z_EC_CENTROLOG
						     WHERE Centro = '${form.FInconIni[o].CentroLogistico.value.toString()}'
					      """
           query191=sql.runSQLFast(query19, false, false, 1).rows
           FInconIni.add(query191.get(0).get("CENTROLOGISTICO"))
            
           //Almacén
           def query20 = """SELECT NOMBREALMACEN
                             FROM Z_EC_ALMACEN
                             WHERE SIGLA  = '${form.FInconIni[o].Almacen.value.toString()}'
                         """      
           query201=sql.runSQLFast(query20, false, false, 1).rows
           FInconIni.add(query201.get(0).get("NOMBREALMACEN"))   
            
           //Material
           FInconIni.add(form.FInconIni[o].Material.value.toString())
            
           //Tipo Material
           FInconIni.add(form.FInconIni[o].TipoMaterial.value.toString())
            
           //Documento Inventario
           FInconIni.add(form.FInconIni[o].Documento.value.toString())
            
           //Unidad de Medida
           FInconIni.add(form.FInconIni[o].UnidadMed.value.toString())
            
           //Valor Unitario
           FInconIni.add(form.FInconIni[o].ValorUni.value.toString())
            
           //Cantidad Sistema
           FInconIni.add(form.FInconIni[o].CantidadSis.value.toString())
            
           //Cantidad Física
           FInconIni.add(form.FInconIni[o].CantidadFi.value.toString())
            
           //Diferencias en Cantidad
           FInconIni.add(form.FInconIni[o].DiferenCan.value.toString())
            
           //Valor Diferencias
           FInconIni.add(form.FInconIni[o].DiferenVal.value.toString())
            
           archivoexcel.getWorksheetByName(FINCONINI).writeRow(FInconIni,count6++)
        }
        //Escribe datos Pestaña SINCONFIN
		for(int p = 0; p < form.SInconFin.size();p++)
		{
           List<String> SInconFin = new ArrayList<String>() 
            
           //fecha del documento
           SInconFin.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
           
           //Centro Logístico
           def query21 = """SELECT CONCAT(CONCAT(Centro,'-'),NombreCentro) AS CENTROLOGISTICO
						 	 FROM Z_EC_CENTROLOG
						     WHERE Centro = '${form.SInconFin[p].CentroLogistico.value.toString()}'
					      """
           query211=sql.runSQLFast(query21, false, false, 1).rows
           SInconFin.add(query211.get(0).get("CENTROLOGISTICO"))
            
           //Almacén
           def query22 = """SELECT NOMBREALMACEN
                             FROM Z_EC_ALMACEN
                             WHERE SIGLA  = '${form.SInconFin[p].Almacen.value.toString()}'
                         """      
           query221=sql.runSQLFast(query22, false, false, 1).rows
           SInconFin.add(query221.get(0).get("NOMBREALMACEN"))   
            
           //Material
           SInconFin.add(form.SInconFin[p].Material.value.toString())
            
           //Tipo Material
           SInconFin.add(form.SInconFin[p].TipoMaterial.value.toString())
            
           //Documento Inventario
           SInconFin.add(form.SInconFin[p].Documento.value.toString())
            
           //Valor Unitario
           SInconFin.add(form.SInconFin[p].ValorUni.value.toString())
            
           //Cantidad Sistema
           SInconFin.add(form.SInconFin[p].CantidadSis.value.toString())
            
           //Cantidad Física
           SInconFin.add(form.SInconFin[p].CantidadFi.value.toString())
            
           //Diferencias en Cantidad
           SInconFin.add(form.SInconFin[p].DiferenCan.value.toString())
            
           //Valor Diferencias
           SInconFin.add(form.SInconFin[p].DiferenVal.value.toString())
           
           archivoexcel.getWorksheetByName(SINCONFIN).writeRow(SInconFin,count7++)
        }           
		       
		//Escribe datos Pestaña FINCONFIN
		for(int q = 0; q < form.FInconFin.size();q++)
		{
           List<String> FInconFin = new ArrayList<String>() 
            
           //fecha del documento
           FInconFin.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
           //Centro Logístico
           def query23 = """SELECT CONCAT(CONCAT(Centro,'-'),NombreCentro) AS CENTROLOGISTICO
						 	 FROM Z_EC_CENTROLOG
						     WHERE Centro = '${form.FInconFin[q].CentroLogistico.value.toString()}'
					      """
           query231=sql.runSQLFast(query23, false, false, 1).rows
           FInconFin.add(query231.get(0).get("CENTROLOGISTICO"))
            
           //Almacén
           def query24 = """SELECT NOMBREALMACEN
                             FROM Z_EC_ALMACEN
                             WHERE SIGLA  = '${form.FInconFin[q].Almacen.value.toString()}'
                         """      
           query241=sql.runSQLFast(query24, false, false, 1).rows
           FInconFin.add(query241.get(0).get("NOMBREALMACEN"))   
            
           //Material
           FInconFin.add(form.FInconFin[q].Material.value.toString())
            
           //Tipo Material
           FInconFin.add(form.FInconFin[q].TipoMaterial.value.toString())
            
           //Documento Inventario
           FInconFin.add(form.FInconFin[q].Documento.value.toString())
            
           //Valor Unitario
           FInconFin.add(form.FInconFin[q].ValorUni.value.toString())
            
           //Cantidad Sistema
           FInconFin.add(form.FInconFin[q].CantidadSis.value.toString())
            
           //Cantidad Física
           FInconFin.add(form.FInconFin[q].CantidadFi.value.toString())
            
           //Diferencias en Cantidad
           FInconFin.add(form.FInconFin[q].DiferenCan.value.toString())
            
           //Valor Diferencias
           FInconFin.add(form.FInconFin[q].DiferenVal.value.toString())
            
           archivoexcel.getWorksheetByName(FINCONFIN).writeRow(FInconFin,count8++)
        }
            
		//Escribe datos Pestaña ACCREQ
        for(int r = 0; r < form.AccReq.size();r++)
		{
            List<String> AccReq = new ArrayList<String>() 
            
            //fecha del documento
            AccReq.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
            //Accion
            AccReq.add(form.AccReq[r].Accion.value.toString())
            
            //Responsable
            AccReq.add(users.getUserById(form.AccReq[r].Responsable.value as long)?.displayName)
            
            //FechaProy
            AccReq.add((form.AccReq[r].FechaProy.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.AccReq[r].FechaProy.value.toString()))))
            
            //Observaciones
            AccReq.add(form.AccReq[r].Observaciones.value.toString())
            
            //Estado
            AccReq.add(form.AccReq[r].Estado.value.toString())
            
            archivoexcel.getWorksheetByName(ACCREQ).writeRow(AccReq,count9++)
        }
             
        //Escribe datos Pestaña HALLUBI
		for(int s = 0; s < form.HallUbi.size();s++)
		{
            List<String> HallUbi = new ArrayList<String>() 
            
            //fecha del documento
            HallUbi.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
            //Código
            HallUbi.add(form.HallUbi[s].Codigo.value.toString())
            
            //Nombre Material	
            HallUbi.add(form.HallUbi[s].NomMat.value.toString())
            
            //Hallazgos
			HallUbi.add(form.HallUbi[s].Hallazgos.value.toString())
            
            archivoexcel.getWorksheetByName(HALLUBI).writeRow(HallUbi,count10++)
        }
            
		//Escribe datos Pestaña HALLPRE
		for(int t = 0; t < form.HallPre.size();t++)
		{
            List<String> HallPre = new ArrayList<String>() 
            
            //fecha del documento
            HallPre.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
            //Código
            HallPre.add(form.HallPre[t].Codigo.value.toString())
            
            //Nombre Material	
            HallPre.add(form.HallPre[t].NomMat.value.toString())
            
            //Hallazgos
			HallPre.add(form.HallPre[t].Hallazgos.value.toString())
            
            archivoexcel.getWorksheetByName(HALLPRE).writeRow(HallPre,count11++)
        }
            
		//Escribe datos Pestaña PART
		for(int u = 0; u < form.Part.size();u++)
		{
            List<String> Part = new ArrayList<String>() 
            
            //fecha del documento
            Part.add((form.FechaDocumento.value)==null ? "Sin Datos" :(sdf2.format(sdf.parse(form.FechaDocumento.value.toString()))))
            
            //Nombre
            Part.add(form.Part[u].Nombre.value.toString())
            
            //Rol
            Part.add(form.Part[u].Rol.value.toString())
            
            archivoexcel.getWorksheetByName(PART).writeRow(Part,count12++)
        }
    }
    }
    response.file(archivoexcel.save(), true)

}
catch(e){
    log.error("---ERROR Reporte GAB34: {}",e)
}
