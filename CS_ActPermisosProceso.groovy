
//EIM Developer: José Barrantes

log.error("Script que actualiza el expediente de acuerdo a diferentes condiciones...")

log.error("Inicio actualizar permisos...")

//variable para obtener el estado del workflow
def workflowStatus

//variable para obtener los atributos del workflow
def atributos

//variable para capturar la carpeta de adjuntos del workflow
def adjuntosFolder

//obtener datos del workflow
try
{
    log.error("Obteniendo datos del workflow...")
    
    workflowStatus = workflow.getWorkflowStatus(workID)
	//workflowStatus = workflow.getWorkflowStatus(2155469)
	//log.error("workflowStatus: ${workflowStatus}")
    
    //obtener los atributos del workflow
    atributos = workflowStatus.getAttributes()
    
    //obtener la carpeta de adjuntos del workflow
    adjuntosFolder = workflowStatus.getAttachmentsFolder()
}

catch (e)
{
    log.error("Error obteniendo datos del workflow. WF ${workID}",e)
}

//variable para obtener el dataid del binder principal
def binderID

//variable para obtener el dataid del documento que va dentro del workflow, en caso de ser necesario
def docID

//variable para obtener el nombre del formulario del atributo del workflow
def nombreFormulario

//variable para obtener el dataid del binder de asignación
def binderAsigID

//obtener el dataid del binder principal
try
{
    log.error("Obteniendo DataID del binder principal...")
    
    //se ejecuta si el workflow es documental, es decir no cuenta con formularios
    if (workflowStatus.forms.size() == 0) //workflowStatus.forms == [:]
    {
        log.error("El workflow no tiene formularios...")
        
        //se guarda el dataid del documento adjunto en el workflow
        docID = docman.getChildrenFast(adjuntosFolder).get(0).ID
    
    	log.error("doc: ${docID}")
    
    	//si el adjunto es un documento
    	if (asCSNode(docID as long).subtype == 144)
    	{
        	log.error("El adjunto es un documento...")
        	
            //variable para obtener el No Expediente del documento que va dentro del workflow, en caso de ser necesario
        	def idCaso = asCSNode(docID as long)."Solicitud Servicio EC"."No Expediente".toString()
    
    		log.error("idCaso: ${idCaso}")
    
    		//query para obtener el dataid del binder principal
    		def queryBinderPrincipal = """select binderid
								  	  	  from z_li_gab40
								  	  	  where idcaso = '${idCaso}'
							   	   	   """
    		//se ejecuta el query queryBinderPrincipal y el resultado se guarda en la variable binderID
    		binderID = sql.runSQLFast(queryBinderPrincipal,false,false,0).rows.collect{it.BINDERID}.get(0)
    	}
    
    	//si el adjunto es un shortcut
    	if (asCSNode(docID as long).subtype == 1)
    	{
        	log.error("El adjunto es un shortcut...")
        
        	//query para obtener el dataid del documento original
        	def queryOrigen = """select origindataid origen
							 	 from dtree 
							  	 where dataid = ${docID} 
  							 	 and deleted = 0
						  	  """
        	//variable para obtener el dataid del documento original
        	def docOrigen = sql.runSQLFast(queryOrigen,false,false,0).rows.collect{it.ORIGEN}.get(0)

        	log.error("docOrigen: ${docOrigen}")
        
        	binderID = asCSNode(-asCSNode(docOrigen as long).parent.parent.parent.ID as long).ID
    	}
	}
    
    //si el workflow si tiene formularios
    else
    {
        log.error("El workflow tiene formularios...")
        
        //log.error("Formularios: ${workflowStatus.forms}")
        
        log.error("Atributo NombreFormulario: ${atributos.data.NombreFormulario}")
        
        //se iguala la variable nombreFormulario al valor del atributo NombreFormulario del workflow
        nombreFormulario = atributos.data.NombreFormulario.toString()        
        
        log.error("nombreFormulario: ${nombreFormulario}")
        
        //Si el formulario es el gab40
        if (nombreFormulario == "WT_GAB40 Solicitud LI")
        {
            log.error("Formulario GAB40...")
            
            //Se obtiene el dataid del binder principal
            binderID = workflowStatus.forms."${nombreFormulario}"."BinderID".toString()
        }
        
        //Si el formulario es el gab51
        if (nombreFormulario == "GAB51")
        {
            log.error("Formulario GAB51...")
            
            //se obtiene el dataid del binder de asignación
            binderAsigID = workflowStatus.forms."${nombreFormulario}"."BinderID".toString()
            
            log.error("binderAsigID: ${binderAsigID}")
            
            //Se obtiene el dataid del binder principal
            binderID = -asCSNode(binderAsigID as long).parent.ID
        }
	}
    
	log.error("binderID: ${binderID}")
}

catch (e)
{
    log.error("Error obteniendo DataID del binder principal. WF ${workID}",e)
}

//variable para guardar los permisos que se deben colocar
def permisos

//variable para obtener el grupo de profesionales EC
def grupo

//variable para almacenar el nombre del rol de profesionales EC
def nombreRol

//variable para almacenar el id del rol de profesionales EC
def rolID

//variable para obtener el rol de profesionales EC
def rol

//variable para almacenar el rol de profesionales EC y el grupo en una lista
def miembros = []

//obtener datos del grupo y del rol
try
{
    log.error("Obteniendo datos del grupo EC y del rol EC...")
    
    //llamar archivo de constantes
    app = docman.getScriptByNickname("am_logisticainversa_config").getCSVars()?.app
    
    //se iguala a la variable a la constante declarada en el archivo general de constantes
    permisos = app.constants.permisosRechazo
    log.error("Permisos a actualizar: ${permisos}")
    
    //se obtiene el grupo de profesionales de LI
    grupo = users.getMemberById(app.constants.grupoLI as long)
    log.error("grupo: ${grupo}")
    
    //se agrega el grupo a miembros
    miembros.add(grupo)
    log.error("grupo agregado: ${miembros}")
    
    //se obtiene el nombre del rol de profesionales de LI
    nombreRol = app.constants.rolLI
	log.error("nombreRol: ${nombreRol}")
    
    //query para obtener el ID del rol de profesionales de LI
    def queryRolID = """select id
						from kuaf
						where type = ${binderID}
						and name = '${binderID}'||'_'||'${nombreRol}'
						-- order by id desc
					 """
    //log.error("queryRolID: ${queryRolID}")
    
    //query para obtener el ID del rol de profesionales de LI para el binder de asignación
    def queryRolAsigID = """select id
							from kuaf
							where type = ${binderID}
							and name = '${binderAsigID}'||'_'||'${nombreRol}'
					 	 """
    //log.error("queryRolAsigID: ${queryRolAsigID}")
    
    //si el workflow tiene formularios
    if (workflowStatus.forms.size() > 0)
    	{
        	if (nombreFormulario == "GAB51")
        	{
            	log.error("Calculando rol para GAB51...")
                
                //se ejecuta el query, el resultado se le asigna a la variable rolID
    			rolID = sql.runSQL(queryRolAsigID,false,false,0).rows.collect{it.ID}.get(0)
        	}
            
            else if (nombreFormulario == "WT_GAB40 Solicitud LI")
        	{
                
                log.error("Calculando rol para GAB40...")
                
                //se ejecuta el query, el resultado se le asigna a la variable rolID
    			rolID = sql.runSQL(queryRolID,false,false,0).rows.collect{it.ID}.get(0)
        	}
    	}
        
        //si el workflow no tiene formularios
        else
        {
            
            log.error("Calculando rol para WF documental...")
                
			//se ejecuta el query, el resultado se le asigna a la variable rolID
			rolID = sql.runSQL(queryRolID,false,false,0).rows.collect{it.ID}.get(0)
        }
    
    log.error("rolID: ${rolID}")
    
    //se obtiene el rol de profesionales de LI
	rol = users.getMemberById(rolID as long)
    
    log.error("rol: ${rol}")
    
    //se agrega el rol a miembros
    miembros.add(rol)
    
    log.error("rol agregado: ${miembros}")
}

catch (e)
{
    log.error("Error obteniendo datos del grupo de EC y del rol EC. WF ${workID}",e)
}

//actualizar los permisos al binder principal
try
{
    if (workflowStatus.forms.size() > 0)
    {
        log.error("Actualizando... WF con formulario")
        
        if (nombreFormulario == "GAB51")
        {
            log.error("GAB51...")
            
            actualizarPermisos(binderAsigID,miembros,permisos)
            actualizarHijos(binderAsigID,miembros,permisos)
        }
        
        if (nombreFormulario == "WT_GAB40 Solicitud LI")
        {
            log.error("GAB40...")
            
            actualizarPermisos(binderID,miembros,permisos)
            actualizarHijos(binderID,miembros,permisos)
        }
    }
    
    else
    {
        log.error("Actualizando... WF documental")
        
        actualizarPermisos(binderID,miembros,permisos)
        actualizarHijos(binderID,miembros,permisos)
    }
}

catch (e)
{
	log.error("Error actualizando los permisos. WF ${workID}",e)
}

//función para actualizar los permisos de los nodos
def actualizarPermisos(DataID,participantes,perms)
{
    
    log.error("Actualizando permisos...")
   
    //actualizar permisos para binder principal
    if (asCSNode(DataID as long).subtype == -1 && asCSNode(DataID as long).parent.subtype == 0) //
    {
        log.error("Actualizando binder...")
        
		//se actualizan los permisos DataID positivo
		//docman.revokeRights(asCSNode(binderID as long),miembros,permisos)
		docman.grantRights(asCSNode(DataID as long),participantes,perms)
    	
		//se actualizan los permisos DataID negativo
		//docman.revokeRights(asCSNode(-(binderID as long)),miembros,permisos)
		docman.grantRights(asCSNode(-(DataID as long)),participantes,perms)
        
		log.error("Permisos del binder actualizados...")
    }

    //actualizar permisos para carpetas y binders dentro de binders
    if (asCSNode(DataID as long).subtype == 0)
    {
        
        log.error("Actualizando carpeta...")
    
		//se actualizan los permisos
		//docman.revokeRights(asCSNode(binderID as long),miembros,permisos)
		docman.grantRights(asCSNode(DataID as long),participantes,perms)
    	
		log.error("Permisos de carpeta actualizados...")
    }
    
    //actualizar permisos para binder dentro del binder principal
    if (asCSNode(DataID as long).subtype == -1 && asCSNode(DataID as long).parent.subtype == 310660)
    {
        log.error("Actualizando binder dentro de binder...")
        
        //Se remueven los permisos
        docman.revokeRights(asCSNode(DataID as long),participantes)
        
        //Se agregan los permisos necesarios
		docman.grantRights(asCSNode(DataID as long),participantes,perms)
        
		log.error("Permisos del binder dentro de binder actualizados...")
    }
}

//función para actualizar los permisos de los hijos de los nodos
def actualizarHijos(DataID,participantes,perms)
{

	//variable para guardar los dataids de los hijos que sean carpeta o binder
	def hijosIDs = []
    
    //variable para guardar los dataids de los hijos que sean formularios
	def formulariosIDs = []
    
    //se consultan los hijos del binder principal
    log.error("Consultando hijos...")
    
    //variable para guardar los hijos
    def hijos = docman.getChildrenFast(asCSNode(DataID as long))
    
    log.error("hijos: ${hijos}")
    
    log.error("Consultando dataids de los hijos...")
    
    //ciclo for que agrega los dataids de binders, carpetas y formularios a una lista correspondiente
    for (int i = 0; i < hijos.size(); i++)
    {
        if (hijos.get(i).subtype == -1 || hijos.get(i).subtype == 0) //|| hijos.get(i).subtype == 0 //144
        {
            //se agregan binder y carpetas
            log.error("Agregando binder/carpeta a hijosIDs...")
        	hijosIDs.add(hijos.get(i).getID())
        }
        
        else if (hijos.get(i).subtype == 223) //223 subtype formularios
        {
            //se agregan formularios
            log.error("Agregando formulario a formulariosIDs...")
            formulariosIDs.add(hijos.get(i).getID())
        }
        
        else
        {
            log.error("No se agrega el elemento. No es binder, carpeta o formulario...")
        }
    }
    
    log.error("DataIDs de los hijos: ${hijosIDs}")
    
    log.error("DataIDs de los formularios: ${formulariosIDs}")
    
    if (hijosIDs.size() > 0)
    {
        log.error("Hay hijos por actualizar...")
        
        //ciclo for que actualiza los binders y carpetas que están dentro del binder
    	for (int j = 0; j < hijosIDs.size(); j++)
    	{
            log.error("Actualizando hijos...")
            
        	actualizarPermisos(hijosIDs.get(j),participantes,perms)
    	}
        
        log.error("Hijos actualizados...")
    }
    
    else
    {
        log.error("No hay hijos para actualizar...")
    }
    
    //si hay formularios
    if (formulariosIDs.size() > 0)
    {
        log.error("Hay formularios para borrar...")
        
        //ciclo for que borra formularios
    	for (int k = 0; k < formulariosIDs.size(); k++)
    	{
        	log.error("Borrando formularios...")
            
        	docman.deleteNode(asCSNode(formulariosIDs.get(k) as long))
    	}
        
        log.error("Formularios borrados...")
    }
    
    //si no hay formularios
    else
    {
        log.error("No hay formularios para borrar...")
    }
}
    
log.error("Fin actualizar permisos...")
