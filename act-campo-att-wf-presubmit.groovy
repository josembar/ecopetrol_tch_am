

//Actualizar campo IDCaso con el valor del atributo IDCaso del workflow (el atributo lleva el código FNX)
		try
		{
    		log.error("GAB56 RD: Obteniendo atributos del workflow ${form.getAmWorkID()}")
    		//obtener los atributos del workflow
            def workflowStatus = workflow.getWorkflowStatus(form.getAmWorkID() as long)
    		atributos = workflowStatus.getAttributes()
    		log.error("atributos: ${atributos}")
    
    		log.error("Actualizando campo IDCaso...")
    		// se actualiza el atributo IDCaso con el campo idcaso del GAB54
    		form.idcaso.value = atributos.data.IDCaso
    		//workflowStatus.updateData()
    
    		log.error("El campo IDCaso ahora es: ${formulario.idcaso.value}")
		}

		catch (e)
		{
			log.error("GAB56 RD: error actualizando campo IDCaso. ${form.getAmWorkID()} ",e)
		}